<?php @include('header.php'); ?>
<main>
    <section class="content-contact mb-80s">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="left-contact__page">
                        <div class="intro-contact__page">
                            <div class="logo-contact__page mb-20s">
                                <a href="#" title="">
                                    <img src="theme/assets/images/logo-footer.png" alt="">
                                </a>
                            </div>
                            <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls color-blues">CÔNG TY CỔ PHẦN TRUYỀN THÔNG & CÔNG NGHỆ ONEMORE</h3>
                            <p>Nếu bạn thích công việc của chúng tôi và muốn hợp tác, đừng ngần ngại liên hệ, chúng tôi sẽ liên hệ lại với bạn.</p>
                        </div>
                        <div class="intro-contact__page">
                            <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls color-blues">THÔNG TIN CÔNG TY</h3>
                            <ul class="hotline-mail__contact mb-30s">
                                <li>
                                    <img src="theme/assets/images/img-icon-contact-1.png" alt="">
                                    <div class="info-mail__contact">
                                        <h3 class="fs-15s title-rb__bold color-blues">Hotline</h3>
                                        <p><a href="tel:0964747979" title="">0964 747 979</a></p>
                                    </div>
                                </li>
                                <li>
                                    <img src="theme/assets/images/img-icon-contact-2.png" alt="">
                                    <div class="info-mail__contact">
                                        <h3 class="fs-15s title-rb__bold color-blues">Email</h3>
                                        <p><a href="mailto:contact@onemore.vn" title="">contact@onemore.vn</a></p>
                                    </div>
                                </li>
                            </ul>
                            <ul class="list-office__contact">
                                <li>
                                    <h3 class="title-rb__bold fs-15s mb-10s color-blues">Văn phòng Miền Trung</h3>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 264 Nguyễn Tri Phương, Quận Thanh Khê, Thành Phố Đà Nẵng</p>
                                </li>
                                <li>
                                    <h3 class="title-rb__bold fs-15s mb-10s color-blues">Văn phòng Miền Nam</h3>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 264 Nguyễn Tri Phương, Quận Thanh Khê, Thành Phố Đà Nẵng</p>
                                </li>
                            </ul>
                        </div>
                        <div class="intro-contact__page">
                            <h3 class="title-rb__bold titles-transform__alls fs-17s mb-25s color-blues">KẾT NỐI VỚI CHÚNG TÔI</h3>
                            <ul class="list-app__footer">
                                <li><a href="https://www.facebook.com/onemore.jsc" title=""><img src="theme/assets/images/app-footer-1.svg" alt=""></a></li>
                                <li><a href="https://www.youtube.com/@onemoremediadanang" title=""><img src="theme/assets/images/app-footer-2.svg" alt=""></a></li>
                                <li><a href="https://www.tiktok.com/@onemore_channel" title=""><img src="theme/assets/images/app-footer-3.svg" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-footer-4.svg" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-footer-5.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="contact-main" id="form-quote-main">
                        <div class="text-top__main titles-center__alls mb-35s">
                            <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                        </div>
                        <form class="mb-30s">
                            <div class="row gutter-20">
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                                </div>
                                <div class="col-lg-12">
                                    <p class="fs-19s color-blues">Lựa chọn dịch vụ</p>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Branding
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Marcom
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Giải pháp marketing tổng thể
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                                </div>
                                <div class="col-lg-6">
                                    <div class="up-file__prd">
                                        <input type="file" name="" class="input-files">
                                        <p class="btn-Choose__file">
                                            Gửi file đính kèm
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                                </div>
                            </div>
                            <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>