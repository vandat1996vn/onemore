<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-3.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Thiết kế ấn phẩm truyền thông</h2>
                <p class="color-text__third">Dịch vụ tư vấn và cung cấp chiến lược toàn diện, phù hợp với mô hình kinh doanh để kiến tạo nền tảng phát triển vững mạnh cho doanh nghiệp. </p>
                <a href="#form-quote-main" title="" class="btn-blue__alls btn-to__form titles-transform__alls">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Tầm quan trọng của marketing tổng thể</h2>
                <p>Marketing tổng thể là chiến lược marketing toàn diện về mọi mặt của một doanh nghiệp, bao gồm từ bước cơ bản nhất là nghiên cứu thị trường đến giai đoạn đo lường kết quả. Marketing tổng thể đóng vai trò rất quan trọng trong việc thực hiện các hoạt động marketing nói riêng và phát triển doanh nghiệp nói chung. Dưới đây là các yếu tố mà một chiến lược marketing toàn diện có thể đáp ứng.</p>
            </div>
            <div class="img-content__sevice mb-80s">
                <img src="theme/assets/images/img-content-sevice-1.png" alt="">
            </div>
            <div class="list-design__capacity">
                <div class="row justify-content-center gutter-20">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-13.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Cụ thể hóa mục tiêu kinh doanh</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-14.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Đưa ra định hướng và lộ trình triển khai rõ ràng, phù hợp</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-15.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Các hoạt động được hoạch định kỹ lưỡng</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-16.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Chủ động kiểm soát các vấn để phát sinh</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-17.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Phân bổ nguồn lực hiệu quả</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-18.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Đảm bảo các bộ phận đều nắm được vai trò của mình và cùng hướng đến mục tiêu chung</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-19.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Dễ dàng đo lường hiệu quả</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advise-maketing__pages mb-80s">
        <img src="theme/assets/images/bg-advise-maketing.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s mb-20s">Thúc đẩy tăng trưởng kinh doanh ngay hôm nay</h2>
            <p>Liên hệ với chúng Onemore để được tư vấn về giải pháp marketing tổng thể</p>
            <a href="#form-quote-main" title="" class="btn-red__alls">NHẬN TƯ VẤN</a>
        </div>
    </section>
    <section class="design-logo__sevice">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Tầm quan trọng của marketing tổng thể</h2>
                <p>Gói Giải pháp Marketing tổng thể của Onemore đề ra và triển khai một chiến lược marketing toàn diện cho doanh nghiệp, đảm bảo các hoạt động marketing được thực hiện đồng bộ theo hệ thống. Từ đó, giúp doanh nghiệp đạt được mục tiêu kinh doanh như kỳ vọng.</p>
            </div>
            <div class="solution-package__benefits">
                <div class="text-top__main titles-center__alls mb-70s">
                    <h2 class="title-hara color-blues fs-38s mb-10s">Những lợi ích nổi bật mà gói Giải pháp của Onemore có thể mang lại bao gồm</h2>
                </div>
                <div class="benefit-design__brochure">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-90s">
                                <img src="theme/assets/images/img-benefit-brochure-1.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s">Chiến lược được hoạch định bởi các đội ngũ marketing có chuyên môn cao, giàu kinh nghiệm.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-90s">
                                <img src="theme/assets/images/img-benefit-brochure-2.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s">Giúp doanh nghiệp tối ưu chi phí cả về ngân sách và nguồn nhân lực.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-90s">
                                <img src="theme/assets/images/img-benefit-brochure-3.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s">Các hạng mục có thể linh hoạt điều chỉnh dựa trên quy mô và tình hình thực tế của từng doanh nghiệp.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-90s">
                                <img src="theme/assets/images/img-benefit-brochure-4.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s">Cam kết hiệu quả đạt được của toàn bộ chiến dịch thực thi.</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="img-content__sevice mb-80s">
                <img src="theme/assets/images/img-content-sevice-2.png" alt="">
            </div>
        </div>
    </section>
    <section class="design-logo__sevice subject-marketing mb-80s">
        <div class="top-design__catalouge">
            <div class="container">
                <div class="text-top__main titles-center__alls mb-50s">
                    <h2 class="title-hara color-blues fs-38s mb-10s"> Gói Giải pháp Marketing tổng thể dành cho ai </h2>
                    <p>Gói Giải pháp Marketing tổng thể được Onemore thiết kế phù hợp với các doanh nghiệp</p>
                </div>
                <div class="list-subject__marketing">
                    <div class="row gutter-20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-subject__marketing">
                                <img src="theme/assets/images/img-subject-marketing-1.png" alt="">
                                <h3 class="fs-21s title-rb__bold color-blues">Doanh nghiệp có quy mô vừa và nhỏ</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-subject__marketing">
                                <img src="theme/assets/images/img-subject-marketing-2.png" alt="">
                                <h3 class="fs-21s title-rb__bold color-blues">Đang trong giai đoạn phát triển/tái định vị thương hiệu</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-subject__marketing">
                                <img src="theme/assets/images/img-subject-marketing-3.png" alt="">
                                <h3 class="fs-21s title-rb__bold color-blues">Ngân sách cho hoạt động truyền thông còn hạn chế</h3>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-subject__marketing">
                                <img src="theme/assets/images/img-subject-marketing-4.png" alt="">
                                <h3 class="fs-21s title-rb__bold color-blues">Chưa có bộ phận marketing/truyền thông in-house chuyên nghiệp</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="register-sevice__main">
                <h3 class="title-hara  fs-22s mb-10s">Gia tăng doanh số cùng Giải pháp Marketing tổng thể ngay!!</h3>
                <a href="#" title="" class="btn-blue__trans titles-transform__alls">ĐĂNG KÝ DỊCH VỤ</a>
            </div>
        </div>
    </section>
    <section class="brand-partner__pages mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-3">
                    <h2 class="title-hara color-blues fs-26s">5000+ Đối tác đã tin tưởng hợp tác</h2>
                </div>
                <div class="col-lg-9">
                    <div class="sl-brand__partner swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="new-main mb-115s">
        <div class="container">
            <div class="top-new__main mb-30s">
                <h2 class="title-hara color-blues fs-38s">Tin tức & kiến thức</h2>
                <a class="btn-red__alls" href="#">Xem thêm</a>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-new__main">
                        <div class="img-new__main">
                            <a href="#" title="">
                                <img src="theme/assets/images/img-new-1.png" alt="">
                            </a>
                        </div>
                        <div class="intro-new__main">
                            <h3><a href="#" title="" class="title-new__main title-rb__bold fs-16s mb-10s">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                            <a href="#" title="" class="see-item__new">xem thêm &rarr;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>