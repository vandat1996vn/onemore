<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-6.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Head Quốc Tiến Đà Nẵng</h2>
                <p class="color-text__third mb-50s"> Thành lập từ năm 2009, thương hiệu thời trang nữ 92Wear đã giành được rất nhiều sự yêu mến từ các bạn trẻ Hà Thành.<br>
                    Từ khởi đầu với cửa hàng quần áo online, đến nay thương hiệu 92Wear đã mở rộng lên 8 cơ sở trên khắp cả nước. </p>
                <ul class="client-sevice__use">
                    <li>
                        <p class="title-client__sevice mb-20s">Khách hàng:</p>
                        <ul>
                            <li>Head Quốc Tiến Đà Nẵng</li>
                        </ul>
                    </li>
                    <li>
                        <p class="title-client__sevice mb-20s">Dịch vụ:</p>
                        <ul>
                            <li>Social Media</li>
                            <li>Logo</li>
                            <li>Digital Marketing</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="container  mb-80s">
        <div class="benefit-design__trademark">
            <div class="row gutter-40">
                <div class="col-lg-5">
                    <div class="img-design__trademark">
                        <img src="theme/assets/images/img-project-details-1.png">
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="intros-design__trademark color-text__third">
                        <h2 class="title-rb__bold color-blues fs-38s mb-20s">Bối Cảnh Thực Hiện Dự Án Head Quốc Tiến Đà Nẵng</h2>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="image-project__details mb-80s">
        <div class="top-image__project">
            <div class="container">
                <p class="title-rb__bold mb-20s">MỤC TIÊU DỰ ÁN</p>
                <h2 class="title-rb__bold color-blues fs-38s mb-20s">Thể hiện hình ảnh chuyên nghiệp, hiện đại để thu hút đối tượng khách hàng trẻ tuổi.</h2>
            </div>
        </div>
        <div class="list-image__project">
            <img src="theme/assets/images/img-prj-detail-1.png" alt="">
            <img src="theme/assets/images/img-prj-detail-2.png" alt="">
            <img src="theme/assets/images/img-prj-detail-3.png" alt="">
        </div>
    </section>
    <section class="container mb-80s">
        <div class="benefit-design__trademark">
            <div class="row gutter-40">
                <div class="col-lg-7">
                    <div class="intros-design__trademark color-text__third">
                        <h2 class="title-rb__bold color-blues fs-38s mb-20s">Kết Quả</h2>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                        <p>Năm 2015 – 2016, đồng loạt các thương hiệu nước ngoài: Zara, H&M,… tấn công vào thị trường thời trang Việt khiến các hãng thời trang nội địa bị cạnh tranh gay gắt.</p>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="img-design__trademark">
                        <img src="theme/assets/images/img-project-details-1.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="value-project__details">
        <div class="container">
            <div class="list-parameter__reason">
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">100000+</h3>
                    <p>Đạt hơn 100000 lượt tiếp cận khách hàng</p>
                    <p>Đạt hơn 100000 lượt tiếp cận khách hàng</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">86+</h3>
                    <p>Doanh thu X10 khi áp dụng
                        hình ảnh thiết kế mới</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">10+</h3>
                    <p>20000 lượt theo dõi
                        khi thực hiện chiến dịch</p>
                </div>
            </div>
        </div>
    </section>
    <section class="advise-maketing__pages mb-130s">
        <img src="theme/assets/images/bg-advise-maketing.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s mb-20s">Bắt đầu dự án tuyệt vời của bạn ngay hôm nay</h2>
            <p>Liên hệ với chúng Onemore để được tư vấn trực tiếp</p>
            <a href="#" title="" class="btn-red__alls">NHẬN TƯ VẤN</a>
        </div>
    </section>s
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>