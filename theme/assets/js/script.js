var GUI = (function() {
    var win = $(window);
    var html = $('html,body');

    var menuMobile = function() {

        if (win.width() <= 991) {
            $('.content-menus').removeClass("menu-desktop");
            $('.content-menus').addClass("main-menu-mobile");
        } else {
            $('.content-menus').addClass("menu-desktop");
            $('.content-menus').removeClass("main-menu-mobile");
        }

        $(document).ready(function($) {
            $('.button-phone').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').toggleClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').addClass("active-close__menussss");
                $('.content-header').toggleClass("active-content__header");
            });
            $('.btn-menu__mopbiles').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').toggleClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').addClass("active-close__menussss");
                $('.content-header').toggleClass("active-content__header");
            });
            $('.bg-over-menu').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').removeClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $('.close-menu-btn').removeClass("active-close__menussss");
                $('.content-header').removeClass("active-content__header");
                $('.main-menu-mobile ul').find('li i').removeClass("active");
                $('.main-menu-mobile ul').find('li').removeClass("actives-li__mobiles");
                $('.main-menu-mobile ul').find('ul').slideUp("fast");
            });
            $('.close-menu-btn').on('click', function(event) {
                $('.animated-icon1').toggleClass('open');
                $('.bg-over-menu').toggleClass('show-over');
                $('.main-menu-mobile').removeClass('active-menu-mobile');
                $('body').toggleClass('overflow-hidden');
                $(this).removeClass("active-close__menussss");
                $('.content-header').removeClass("active-content__header");
                $('.main-menu-mobile ul').find('li i').removeClass("active");
                $('.main-menu-mobile ul').find('li').removeClass("actives-li__mobiles");
                $('.main-menu-mobile ul').find('ul').slideUp("fast");
            });
        });

        $('.main-menu-mobile').find("ul li").each(function() {
            if ($(this).find("ul>li").length > 0) {
                $(this).prepend('<i></i>');
                $(this).append('<span></span');
            }
        });

        $('.menu-desktop').find("ul li").each(function() {
            if ($(this).find("ul>li").length > 0) {
                $(this).children("a").append('<i></i>');
            }
        });

        $('.main-menu-mobile ul').find('li i').click(function(event) {
            var ul = $(this).nextAll("ul");
            if (ul.is(":hidden")) {
                $(this).addClass('active');
                ul.slideDown(200);
            } else {
                $(this).removeClass('active');
                ul.slideUp();
            }
        });
    };

    var _scroll_menus = function() {
        var win = $(window);
        var heightSloganHeader = $('.news-slogan__header').outerHeight();
        var heighttops = $('.header').outerHeight();
        var prevScrollpos = window.pageYOffset;

        $('body').css('padding-top', heighttops);


        if ($("body").find(".main-infos").length > 0) {
            $("header").addClass("header-mains");

            $('body').css('padding-top', '0px');
        }


        window.onscroll = function() {

            var currentScrollPos = window.pageYOffset;

            if (win.scrollTop() >= heighttops) {

                $('.header').addClass('scroll-fixed__headers');

                $('.header').css('top', (0 - heightSloganHeader));



                if (win.width() <= 575) {


                    if (prevScrollpos > currentScrollPos) {

                        $('.header').css('top', 0);
                    } else {

                        $('.header').css('top', (0 - heighttops));
                    }
                    prevScrollpos = currentScrollPos;
                }

            } else {
                $('.header').css('top', 0);
                $('.header').removeClass('scroll-fixed__headers');
                $('.btn-fix__rights').removeClass('active-fix__rights');
            }
        }

    };




    var slideMains = function() {
        if ($('.banner-mains').length === 0) return;
        var swiper2 = new Swiper('.banner-mains', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: ".swiper-pagination",
            },

        });
    };

    var slideFeedbackMain = function() {
        if ($('.feedback-slide__main').length === 0) return;
        var swiper2 = new Swiper('.feedback-slide__main', {
            slidesPerView: "auto",
            spaceBetween: 30,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 5
                },
                576: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: "auto",
                    spaceBetween: 10
                },
            }

        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parents(".feekback-main").find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parents(".feekback-main").find(".swiper .swiper-button-next").trigger("click");
        });

    };

    var slideCapacityFile = function() {
        if ($('.sl-capacity__file').length === 0) return;
        var swiper2 = new Swiper('.sl-capacity__file', {
            slidesPerView: 3,
            spaceBetween: 30,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 5
                },
                576: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
            }

        });


        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });

    };

    var slidebrandPartner = function() {
        if ($('.sl-brand__partner').length === 0) return;
        var swiper2 = new Swiper('.sl-brand__partner', {
            slidesPerView: 4,
            spaceBetween: 70,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 4,
                    spaceBetween: 10
                },
                576: {
                    slidesPerView: 5,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 5,
                    spaceBetween: 30
                },
                992: {
                    slidesPerView: 4,
                    spaceBetween: 70
                },
            }
        });

    };

    var slidePassionAbout = function() {
        if ($('.sl-passion__about').length === 0) return;
        var swiper2 = new Swiper('.sl-passion__about', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            draggable: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });

        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });

    };

    var slideMediaPersonnel = function() {
        if ($('.sl-media__personnel').length === 0) return;
        var swiper2 = new Swiper('.sl-media__personnel', {
            slidesPerView: 3,
            grid: {
                rows: 2,
            },
            spaceBetween: 0,
            loop: false,
            draggable: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },



        });

        $(".showss-button-prev").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-prev").trigger("click");
        });

        $(".showss-button-next").click(function() {
            $(this).parent(".group-btns__showss").parent().find(".swiper .swiper-button-next").trigger("click");
        });

    };

    var file_forms = function() {
        $(".input-files").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).parents(".up-file__prd").find(".btn-Choose__file").addClass("selected").html(fileName);
        });
    }

    function allHeights() {
        $('.list-process__all').each(function() {
            $(this).find('.items-process__all').css('height', '');
            var heightAlls = 0
            $(this).find('.items-process__all').each(function() {
                if ($(this).height() > heightAlls) {
                    heightAlls = $(this).height();
                }
            })
            $(this).find('.items-process__all').height(heightAlls);
        });

        $('.row').each(function() {
            $(this).find('.item-standard__all').css('height', '');
            var heightAllsStandard = 0
            $(this).find('.item-standard__all').each(function() {
                if ($(this).height() > heightAllsStandard) {
                    heightAllsStandard = $(this).height();
                }
            })
            $(this).find('.item-standard__all').height(heightAllsStandard);
        });

        $('.row').each(function() {
            $(this).find('.item-standard__all .title-rb__bold').css('height', '');
            var heightAllTitle = 0
            $(this).find('.item-standard__all .title-rb__bold').each(function() {
                if ($(this).height() > heightAllTitle) {
                    heightAllTitle = $(this).height();
                }
            })
            $(this).find('.item-standard__all .title-rb__bold').height(heightAllTitle);
        });


        $('.row').each(function() {
            $(this).find('.items-design__trademark .title-design__trademark').css('height', '');
            var heightTitlesElement = 0
            $(this).find('.items-design__trademark .title-design__trademark').each(function() {
                if ($(this).height() > heightTitlesElement) {
                    heightTitlesElement = $(this).height();
                }
            })
            $(this).find('.items-design__trademark .title-design__trademark').height(heightTitlesElement);
        });


        //------------------>
    }

    $(window).on("load resize", function(e) {
        allHeights();
    });


    var scollBtnMains = function() {
        $(".btn-to__form").on("click", function(eventsss) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $("html, body").animate({
                        scrollTop: $(hash).offset().top - 120,
                    },
                    800,
                    function() {
                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash - 120;
                    }
                );
            } // End if
        });
    };

    var ckickShowComment = function() {
        $(".btn-see__cmt").click(function() {
            $(".list-cmt__new").find(".items-cmt__new:nth-child(n+2)").slideDown();
            $(this).fadeOut();
        })
    }



 var listQuestion = function() {
        $(".active-questions").find(".info-mar__faq").slideDown('fast');

        $(".btn-mar__faq").click(function() {
            $(this).parent("li").find(".info-mar__faq").slideToggle('fast');
            $(this).parent("li").toggleClass("active-questions");
        });
    };



    var scrollTopss = function() {
        $(".btn-up__tops").click(function() {
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
    };

    var initWowJs = function() {
        new WOW().init();
    };

    var editor = function() {
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    };


    /*  Mobile */

    return {
        _: function() {
            _scroll_menus();
            menuMobile();
            slideMains();
            slideFeedbackMain();
            slideCapacityFile();
            slidebrandPartner();
            slidePassionAbout();
            slideMediaPersonnel();
            scollBtnMains();
            ckickShowComment();
            allHeights();
            file_forms();
            editor();
            listQuestion();
            scrollTopss();
            initWowJs();
        }
    };


})();
$(document).ready(function() {
    // if (/Lighthouse/.test(navigator.userAgent)) {
    //     return;
    // }
    GUI._();
});