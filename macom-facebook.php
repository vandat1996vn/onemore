<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-mar-facebook.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Dịch vụ Facebook Ads tại Onemore</h2>
                <p class="color-text__third">Chiến lược rõ ràng giúp bạn phát triển doanh nghiệp của mình. Triển khai chiến dịch chuyên nghiệp mang đến hiệu quả cao. Báo cáo kết quả minh bạch cho thấy lợi tức đầu tư của bạn </p>
                <a href="#form-quote-main" title="" class="btn-blue__alls btn-to__form titles-transform__alls">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-80s">
        <div class="container">
            <h2 class="titles-center__alls title-hara color-blues fs-38s mb-50s">Những vấn đề mà bạn đang gặp phải?</h2>
            <div class="img-marquest__face mb-30s">
                <img src="theme/assets/images/mar-quest-facebook.png" alt="">
            </div>
            <a href="#" title="" class="btn-red__alls m-auto mb-25s">NHẬN TƯ VẤN</a>
        </div>
    </section>
    <section class="design-logo__sevice manufacturing-videos__marketing mb-130s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> Lý do chọn dịch vụ Facebook Ads tại Onemore </h2>
            </div>
            <div class="list-design__catalouge list-standard__all mb-30s">
                <div class="row gutter-20 justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-marcom-facebook-icons-1.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Đưa ra kế hoạch chiến lược toàn diện</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Chúng tôi phát triển chiến lược tổng thể phù hợp với mục tiêu kinh doanh của bạn, bao gồm cả khả năng xác định đối tượng khách hàng tiềm năng và các kênh quảng cáo hiệu quả.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-marcom-facebook-icons-2.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Hỗ trợ bạn giảm thiểu thời gian và chi phí</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Sử dụng dịch vụ quảng cáo Facebook tại Onemore giúp bạn tiết kiệm thời gian và chi phí khi quảng cáo sản phẩm và dịch vụ của mình.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-marcom-facebook-icons-3.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Cam kết tăng doanh số sau một tháng </h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Dịch vụ quảng cáo Facebook cam kết tăng doanh số của doanh nghiệp sau một tháng sử dụng dịch vụ.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-marcom-facebook-icons-4.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Tối ưu hóa hoạt động Sales và Marketing cho doanh nghiệp của bạn</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Dịch vụ quảng cáo Facebook cung cấp các giải pháp tối ưu cho hoạt động Marketing và Sales của doanh nghiệp, giúp tăng tỷ lệ chuyển đổi và doanh số bán hàng.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-marcom-facebook-icons-5.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Tập trung vào việc phát triển doanh nghiệp</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Việc sử dụng dịch vụ quảng cáo Facebook giúp doanh nghiệp tập trung vào việc phát triển doanh nghiệp của mình, Onemore sẽ loại bỏ những vấn đề về quảng cáo, giúp bạn dành nhiều thời gian hơn để tập trung vào hoạt động kinh doanh chính.  </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="chitiettuyendung.php" title="" class="btn-red__alls m-auto mb-25s">NHẬN TƯ VẤN</a>
        </div>
    </section>
    <section class="design-logo__sevice  mb-115s">
        <div class="process-design__logo">
            <img src="theme/assets/images/bg-traemask-mar-facebook.png" alt="">
            <div class="container">
                <h2 class="title-hara titles-center__alls color-blues fs-38s mb-30s">Quy trình quảng cáo Facebook tại Onemore</h2>
                <div class="list-process__all">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">1</p>
                                    <p class="titles-transform__alls">BƯỚC 1</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">TIẾP NHẬN YÊU CẦU TỪ KHÁCH HÀNG</h3>
                                    <p>Chúng tôi sẽ liên hệ trực tiếp với khách hàng để hiểu rõ về nhu cầu, mục tiêu và ngân sách quảng cáo.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">2</p>
                                    <p class="titles-transform__alls">BƯỚC 2</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">Tư vấn miễn phí và đề xuất chiến dịch quảng cáo</h3>
                                    <p>Dựa trên yêu cầu của khách hàng, chúng tôi sẽ lên kế hoạch và đề xuất chiến dịch quảng cáo phù hợp, đảm bảo tối ưu hiệu quả và chi phí.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">3</p>
                                    <p class="titles-transform__alls">BƯỚC 3</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">Thực hiện triển khai chiến dịch quảng cáo</h3>
                                    <p>Sau khi được khách hàng chấp nhận kế hoạch, chúng tôi sẽ triển khai chiến dịch quảng cáo trên Facebook với các nội dung và hình ảnh được thiết kế chuyên nghiệp và hấp dẫn.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">4</p>
                                    <p class="titles-transform__alls">BƯỚC 4</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">Theo dõi, đánh giá và tối ưu chiến dịch quảng cáo</h3>
                                    <p>Chúng tôi sẽ theo dõi và đánh giá hiệu quả của chiến dịch quảng cáo, từ đó tối ưu và điều chỉnh các chiến lược quảng cáo để đạt được hiệu quả tốt nhất.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">5</p>
                                    <p class="titles-transform__alls">BƯỚC 5</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">TỔNG KẾT và hỗ trợ sau chiến dịch quảng cáo</h3>
                                    <p>Sau khi chiến dịch quảng cáo hoàn tất và đạt được kết quả như mong đợi, chúng tôi sẽ báo cáo tổng kết chiến dịch và đề xuất phương án phù hợp nhằm đảm bảo hoạt động quảng cáo tiếp tục hoạt động ổn định và hiệu quả trong thời gian dài..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice diversity-mar__facebook mb-80s">
        <div class="container">
            <h2 class="titles-center__alls title-hara color-blues fs-38s mb-10s">Những vấn đề mà bạn đang gặp phải?</h2>
            <p class="titles-center__alls mb-30s">Giúp bạn tiếp cận khách hàng tiềm năng trên facebook bằng nhiều phương thức khác nhau</p>
            <div class="img-marquest__face img-default__center mb-30s">
                <img src="theme/assets/images/diversity-mar-facebook.png" alt="">
            </div>
            <a href="#" title="" class="btn-red__alls m-auto mb-25s">NHẬN TƯ VẤN</a>
        </div>
    </section>
    <section class="all-marcom__sevice mb-50s">
        <div class="container">
            <h2 class="titles-center__alls title-hara color-blues fs-38s mb-35s">Các gói Facebook Ads</h2>
            <div class="row gutter-20">
                <div class="col-lg-6">
                    <div class="box-marcom__sevice marcom-blue__1">
                        <h3 class="titles-center__alls title-rb__bold color-blues fs-21s mb-10s">Dịch vụ quản trị tài khoản
                            Facebook ADS</h3>
                        <p class="mb-30s">Gói dịch vụ được thiết kế để phù hợp với doanh nghiệp vừa và nhỏ.</p>
                        <ul class="list-marcom__service mb-15s">
                            <li>Thiết lập tài khoản chuẩn với chính sách Facebook</li>
                            <li>Cài đặt chiến dịch và theo dõi chuyển đổi</li>
                            <li>Đưa ra các phương án tối ưu hiệu quả để giúp khách hàng đạt được mục tiêu kinh doanh</li>
                            <li>Hệ thống báo cáo chuyên nghiệp</li>
                            <li>Hỗ trợ xử lý các nội dung vi phạm chính sách Facebook</li>
                        </ul>
                        <p class="service-charge__marcom mb-10s">Phí dịch vụ chỉ <span class="fs-36s color-blues title-rb__bold">15%</span>ngân sách</p>
                        <a href="#" class="btn-marcom__sevice">Đăng ký</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="box-marcom__sevice marcom-blue__2">
                        <h3 class="titles-center__alls title-rb__bold color-blues fs-21s mb-10s">Dịch vụ Facebook ADS
                            trọn gói</h3>
                        <p class="mb-30s">Được thiết kế đặc biệt để đáp ứng nhu cầu của những doanh nghiệp có yêu cầu nghiêm ngặt về KPI’s.</p>
                        <ul class="list-marcom__service mb-15s">
                            <li>Thiết lập tài khoản chuẩn với chính sách Facebook</li>
                            <li>Cài đặt và theo dõi các chỉ số đã cam kết</li>
                            <li>Đưa ra các phương án tối ưu hiệu quả để giúp khách hàng đạt được mục tiêu kinh doanh</li>
                            <li>Hệ thống báo cáo chuyên nghiệp</li>
                            <li>Hỗ trợ xử lý các nội dung vi phạm chính sách Facebook</li>
                        </ul>
                        <p class="service-charge__marcom mb-10s">Phí dịch vụ tùy vào KPI’s đề ra</p>
                        <a href="#" class="btn-marcom__sevice">Đăng ký</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-main mb-115s">
        <div class="container">
            <div class="titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Khách hàng đã hợp tác cùng Onemore</h2>
            </div>
            <div class="row gutter-100">
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-mar__sevice mb-80s">
        <div class="container">
            <h2 class="title-hara titles-center__alls color-blues fs-38s mb-30s">Các câu hỏi thường gặp</h2>
            <ul class="list-faq__marcom">
                <li class="active-questions">
                    <p class="btn-mar__faq"> Làm thế nào để chúng tôi đảm bảo rằng chiến dịch Facebook Ads của bạn sẽ đạt hiệu quả? <span class="icons-faq__marcom"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i></span></p>
                    <ul class="info-mar__faq">
                        <li>Chúng tôi sẽ tư vấn và đưa ra kế hoạch chi tiết để tối ưu hoá chiến dịch của bạn, từ lựa chọn đối tượng khách hàng, định dạng quảng cáo, cách thức đặt mục tiêu cho đến đo lường kết quả.</li>
                    </ul>
                </li>
                <li>
                    <p class="btn-mar__faq">Làm sao tôi biết được quảng cáo có đang hiệu quả hay không? <span class="icons-faq__marcom"> <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i></span></p>
                    <ul class="info-mar__faq">
                        <li>Chúng tôi sẽ tư vấn và đưa ra kế hoạch chi tiết để tối ưu hoá chiến dịch của bạn, từ lựa chọn đối tượng khách hàng, định dạng quảng cáo, cách thức đặt mục tiêu cho đến đo lường kết quả.</li>
                    </ul>
                </li>
                <li>
                    <p class="btn-mar__faq"> Ngân sách tối thiểu cho 1 chiến dịch quảng cáo Facebook là bao nhiêu? <span class="icons-faq__marcom"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i></span></p>
                    <ul class="info-mar__faq">
                        <li>Chúng tôi sẽ tư vấn và đưa ra kế hoạch chi tiết để tối ưu hoá chiến dịch của bạn, từ lựa chọn đối tượng khách hàng, định dạng quảng cáo, cách thức đặt mục tiêu cho đến đo lường kết quả.</li>
                    </ul>
                </li>
                <li>
                    <p class="btn-mar__faq"> Chi phí cho dịch vụ này như thế nào? <span class="icons-faq__marcom"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i></span></p>
                    <ul class="info-mar__faq">
                        <li>Chúng tôi sẽ tư vấn và đưa ra kế hoạch chi tiết để tối ưu hoá chiến dịch của bạn, từ lựa chọn đối tượng khách hàng, định dạng quảng cáo, cách thức đặt mục tiêu cho đến đo lường kết quả.</li>
                    </ul>
                </li>
                <li>
                    <p class="btn-mar__faq"> Nếu tôi có thắc mắc hoặc có vấn đề cần hỗ trợ, tôi liên hệ với ai? <span class="icons-faq__marcom"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true"></i></span></p>
                    <ul class="info-mar__faq">
                        <li>Chúng tôi sẽ tư vấn và đưa ra kế hoạch chi tiết để tối ưu hoá chiến dịch của bạn, từ lựa chọn đối tượng khách hàng, định dạng quảng cáo, cách thức đặt mục tiêu cho đến đo lường kết quả.</li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*" fdprocessedid="nq7aa">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*" fdprocessedid="dhwi6">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*" fdprocessedid="1ae2vn">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*" fdprocessedid="27al7b">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls " fdprocessedid="7z0q5l">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>