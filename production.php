<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-4.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Production/ Media</h2>
                <p class="color-text__third"> Marketing tổng thể là chiến lược marketing toàn diện về mọi mặt của một doanh nghiệp, bao gồm từ bước cơ bản nhất là nghiên cứu thị trường đến giai đoạn đo lường kết quả.. </p>
                <a href="#" title="" class="btn-blue__alls titles-blues__alls btn-to__form ">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice manufacturing-videos__marketing mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> Sản xuất video marketing </h2>
            </div>
            <div class="list-design__catalouge list-standard__all">
                <div class="row gutter-20">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-20.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Video marketing là gì?</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Là các video được sản xuất để truyền tải đến khách hàng những thông điệp liên quan đến thương hiệu, sản phẩm hay dịch vụ của doanh nghiệp.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-21.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Lợi ích của video marketing</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Tăng tỉ lệ chuyển đổi</li>
                                    <li>Tạo dấu ấn với khách hàng</li>
                                    <li>Tối ưu hóa hoạt động SEO</li>
                                    <li>Truyền tải thông tin hiệu quả</li>
                                    <li>Tiếp cận khách hàng trên đa phương tiện</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-22.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Các dạng video marketing </h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Video giải thích</li>
                                    <li>Video quảng cáo</li>
                                    <li>Video khuyến mãi, giảm giá</li>
                                    <li>Video giới thiệu cảm nhận của khách hàng</li>
                                    <li>Video giáo dục/kiến thức</li>
                                    <li>Livestream</li>
                                    <li>Video animation</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-23.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Doanh nghiệp trong lĩnh vực nào nên sử dụng gói dịch vụ này?</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Gói giải pháp sản xuất video marketing của Onemore phù hợp với các ngành hoặc lĩnh vực cần sử dụng nhiều video cho các hoạt động truyền thông, quảng cáo.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-23.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Giảm thiểu chi phí quảng cáo</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Hiện nay, video là hình thức marketing xu hướng và đem lại tỷ lệ chuyển đổi cao nhất. Có thể thấy rằng các mạng xã hội và sàn thương mại điện tử đang tích cực phát triển hình thức này. Với mỗi dự án, Onemore luôn cam kết KPI tỷ lệ tổng tương tác tương ứng - một nguồn chuyển đổi quan trọng với doanh nghiệp.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-23.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s" style="height: 50.4167px;">Giảm thiểu chi phí quảng cáo</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Video là hình thức marketing có thể giúp doanh nghiệp vừa và nhỏ đẩy nhanh tốc độ phát triển. Gói giải pháp của Onemore bao gồm 3 hạng mục lần lượt là: gói cơ bản, gói tiêu chuẩn và gói cao cấp. Khách hàng có thể lựa chọn hạng mục sao cho phù hợp nhất với mục tiêu và ngân sách của doanh nghiệp</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="banner-production__media mb-80s">
        <img src="theme/assets/images/bg-production-media.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s">Sản xuất phim truyền thông</h2>
        </div>
    </section>
    <section class="design-logo__sevice mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-70s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Tầm quan trọng của việc sản xuất phim truyền thông</h2>
                <p>Phim truyền thông là những thước phim ngắn được đầu tư kỹ lưỡng về mặt nội dung, hình ảnh nhằm truyền tải đến người xem những thông điệp liên quan đến doanh nghiệp, thương hiệu, sản phẩm/dịch vụ.</p>
            </div>
            <div class="benefit-design__trademark mb-80s">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">So sánh phim và video truyền thông</h2>
                <div class="row gutter-40">
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-film-compare.png">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Phim mang tính chuyên nghiệp hơn video</h3>
                                        <p>Vì đặc thù của thiết bị sản xuất mà hình ảnh, màu sắc của phim sống động và chân thực hơn so với video. Qua đó nội dung được thể hiện cũng trở nên chuyên nghiệp..</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Phim đòi hỏi sự đầu tư trong quá trình thực hiện hơn video</h3>
                                        <p>Nội dung, bối cảnh, âm thanh, hay từng góc máy của phim đều cần sự chỉnh chu nhất định. Vì vậy khi sản xuất phim nên có đội ngũ chuyên nghiệp thực hiện để cho ra kết quả tốt nhất.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Sản xuất phim cần sử dụng các thiết bị phức tạp hơn video</h3>
                                        <p> Khác với việc ai cũng có thể tạo ra video chỉ với một chiếc điện thoại có chức năng quay chụp, để sản xuất một thước phim cần sử dụng các máy móc chuyên dụng và đòi hỏi nhân sự có kỹ năng để điều khiển các máy móc đó.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="benefit-design__trademark">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">Các dạng phim truyền thông</h2>
                <div class="row gutter-40">
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">TVC quảng cáo</h3>
                                        <p>Là video quảng cáo, giới thiệu về sản phẩm/dịch vụ thương mại hay một sự kiện nào đó được phát sóng trên hệ thống truyền hình. Nhờ sức lan tỏa rộng mà video có thể tiếp cận đa dạng đối tượng khán giả và không phụ thuộc vào không gian hay khoảng cách địa lý.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Phim doanh nghiệp</h3>
                                        <p>Là video có thời lượng dưới 10 phút, giới thiệu về công ty, sản phẩm/dịch vụ, cơ sở vật chất hoặc các sự kiện quan trọng. Qua đó giúp người xem hình dung được quá trình hình thành và phát triển của doanh nghiệp.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Video viral</h3>
                                        <p> Là video khiến người xem thích thú và có hành động chia sẻ đến bạn bè, người thân của mình. Nhờ vậy mà video nhanh chóng trở nên phổ biến và đạt lượt xem “khủng” trên các kênh trực tuyến..</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-media-compare.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="brand-partner__pages mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-3">
                    <h2 class="title-hara color-blues fs-26s">5000+ Đối tác đã tin tưởng hợp tác</h2>
                </div>
                <div class="col-lg-9">
                    <div class="sl-brand__partner swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="prj-brand__pages mb-80s">
        <div class="container">
            <h2 class="title-hara titles-center__alls color-blues fs-38s mb-20s">Các dự an Production/ Media đã thực hiện</h2>
            <div class="row mb-20s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" title="" class="btn-see__more">XEM thêm</a>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>