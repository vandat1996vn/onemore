<?php @include('header.php'); ?>
<main>
    <section class="banner-new__details mb-40s">
        <img src="theme/assets/images/banner-new-details.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s mb-15s">Chúng tôi chia sẻ kiến thức và kinh nghiệm làm việc để giúp bạn phát triển</h2>
            <p>Bảng tin Onemore</p>
        </div>
    </section>
    <section class="content-new__details mb-50s">
        <div class="container">
            <div class="row gutter-40">
                <div class="col-lg-8">
                    <div class="top-author__new mb-40s">
                        <div class="author-new__detail">
                            <img src="theme/assets/images/img-author-new.png" alt="">
                            <div class="intros-author__new">
                                <p class="title-rb__bold fs-15s mb-5s">Thi Huỳnh Onemore</p>
                                <p>11:20 Thứ Sáu, 5 tháng 12, 2022</p>
                            </div>
                        </div>
                        <div class="share-view__new">
                            <ul class="share-app__news">
                                <li><a href="#" title=""><img src="theme/assets/images/img-news-share-1.png"></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/img-news-share-2.png"></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/img-news-share-3.png"></a></li>
                            </ul>
                            <p class="view-new__details"><img src="theme/assets/images/eye-news.png" alt=""> 4252 lượt xem</p>
                        </div>
                    </div>
                    <img src="theme/assets/images/table-of-contents.png" alt="" class="mb-50s">
                    <div class="text-news__details mb-70s">
                        <p><strong>Khi biết tin trúng tuyển vào trường đại học, thí sinh cần lưu ý việc chuẩn bị các loại giấy tờ cần thiết khi nộp hồ sơ nhập học đại học 2022.</strong></p>
                        <br>
                        <ul style="padding-left: 45px;">
                            <li><a href="#" title=""><strong>Danh sách các trường Đại học công bố điểm sàn năm 2022</strong></a></li>
                            <li><a href="#" title=""><strong>Danh sách các trường Đại học công bố điểm sàn năm 2022</strong></a></li>
                            <li><a href="#" title=""><strong>Danh sách các trường Đại học công bố điểm sàn năm 2022</strong></a></li>
                        </ul>
                        <br>
                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm và danh sách thí sinh trúng tuyển chính thức trên trang thông tin điện tử của trường và trên phương tiện thông tin đại chúng.</p>
                        <br>
                        <p>TS Nguyễn Thị Kim Phụng, Vụ trưởng Vụ Giáo dục Đại học (Bộ GD&ĐT) cho biết, nếu đã có tên trong danh sách trúng tuyển, thí sinh xác nhận khẳng định nhập học vào trường đã trúng tuyển trong thời hạn quy định bằng cách bản chính Giấy chứng nhận kết quả thi (trực tiếp tại trường hoặc bằng thư chuyển phát nhanh đường Bưu điện).</p>
                        <br>
                        <img src="theme/assets/images/img-new-details.png" alt="">
                        <br>
                        <p class="fs-30s"><strong>1.Những Lưu Ý</strong></p>
                        <br>
                        <p>- <strong>Giấy báo nhập học</strong> phải là bản chính, Bằng tốt nghiệp phải có bản chính và bản sao (có công chứng), bằng tốt nghiệp THPT, bổ túc THPT, trung học chuyên nghiệp hoặc bản chính giấy chứng nhận tốt nghiệp;</p>
                        <br>
                        <p>- Với giấy khai sinh, các bạn phải có bản sao chứ các bạn không được photo công chứng. Đối với Hồ sơ trúng tuyển viên (Hồ sơ nhập học đại học) phải ghi đầy đủ tất cả các mục, dán hình có đóng dấu giáp lai và xác nhận của địa (theo mẫu do Bộ GD-ĐT phát hành, các bạn có thể chỉ có thể mua tại Sở GD-ĐT ở địa phương bạn học cấp III);</p>
                        <br>
                        <p>- Với học bạ bạn cần có bản sao (có công chứng, bạn có thể sao học bạ tại trường cấp III bạn đã theo học);
                            Đối với SV trúng tuyển nhờ xét điểm ưu tiên (đối tượng, khu vực) phải nộp giấy chứng nhận ưu tiên. Giấy chứng nhận chính sách: dân tộc ít người, con liệt sĩ, con thương/bệnh binh (kèm bản sao thẻ thương/bệnh binh có công chứng), nhận hộ nghèo… dùng để xét miễn giảm học phí.</p>
                        <br>
                        <p><strong> Lưu ý :</strong></p>
                        <br>
                        <p> - Thí sinh trúng tuyển phải đến nhập học đại học theo đúng yêu cầu ghi trong Giấy triệu tập trúng tuyển của trường. chậm 15 ngày trở lên (kể từ ngày ghi trong giấy triệu tập trúng tuyển), nếu không có lí do chính đáng, coi như bỏ học. chậm do ốm đau, tai nạn có giấy xác nhận của bệnh viện quận, huyện trở lên hoặc do thiên tai có xác nhận của UBND huyện trở lên, được xem xét vào học hoặc bảo lưu sang năm sau.</p>
                    </div>
                    <div class="signature-new mb-50s">
                        <p>Tin tổng hợp - <a href="#" title=""> Onemore </a></p>
                        <p>Tác giả:<strong>Thi Huỳnh.</strong></p>
                    </div>
                    <div class="bottom-all__new mb-50s">
                        <div class="box-bottom__new">
                            <h3 class="title-rb__bold title-box__bottom title fs-17s">TAGS:</h3>
                            <ul class="bottom-tag__news">
                                <li><a href="#" title="">#Brand Award</a></li>
                                <li><a href="#" title="">#Trending design</a></li>
                                <li><a href="#" title="">#Logo</a></li>
                            </ul>
                        </div>
                        <div class="box-bottom__new">
                            <h3 class="title-rb__bold title-box__bottom title fs-17s">Follow Onemore tại! </h3>
                            <ul class="list-follow__apps">
                                <li><a href="#" title=""><img src="theme/assets/images/app-follow-1.png" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-follow-2.png" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-follow-3.png" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="box-bottom__new">
                            <h3 class="title-rb__bold title-box__bottom title fs-17s">Xếp theo: </h3>
                            <ul class="tag-cmt__new">
                                <li><a href="#" title="" class="active">Mới nhất</a></li>
                                <li><a href="#" title="">Cũ nhất</a></li>
                            </ul>
                            <div class="form-cmt__new mb-30s">
                                <form>
                                    <textarea id="editor" rows="4"></textarea>
                                </form>
                            </div>
                            <div class="list-cmt__new">
                                <div class="items-cmt__new">
                                    <div class="intro-cmt__new">
                                        <img src="theme/assets/images/avatar-news-cmt-1.png" alt="">
                                        <div class="text-cmt__new">
                                            <div class="top-text__cmt">
                                                <div class="name-day__up">
                                                    <p class="title-rb__bold">Lưu Minh Tâm</p>
                                                    <p>2 ngày trước</p>
                                                </div>
                                            </div>
                                            <p class="color-text__third">Cho e hỏi là giấy khai sinh bản sao của e bị mất. Vậy e nộp bản photo của bản chính đc hk ạ</p>
                                            <ul class="like-answer__cmt color-text__third">
                                                <li><i class="fa fa-thumbs-up" aria-hidden="true"></i> 5</li>
                                                <li><i class="fa fa-reply" aria-hidden="true"></i> Trả lời 2</li>
                                            </ul>
                                            <div class="items-cmt__new">
                                                <div class="intro-cmt__new">
                                                    <img src="theme/assets/images/avatar-news-cmt-2.png" alt="">
                                                    <div class="text-cmt__new">
                                                        <div class="top-text__cmt">
                                                            <div class="name-day__up">
                                                                <p class="title-rb__bold">KTS </p>
                                                                <p>2 ngày trước</p>
                                                            </div>
                                                            <p class="answer-cmt title-rb__bold color-text__third"><i class="fa fa-share" aria-hidden="true"></i> Lưu Minh Tâm </p>
                                                        </div>
                                                        <p class="color-text__third">Chào bạn, bạn lên phòng GD&ĐT của địa phương hoặc đến trường THPT để xin cấp lại giấy nhé. Giấy này bạn có thể bổ sung trong vòng 1 tháng kể từ ngày nhập học bạn nhé.</p>
                                                        <br>
                                                        <p><a href="#" title="">https://kenhtuyensinh.vn/ho-so-nhap-hoc-dai-hoc-2022-can-chuan-bi-nhung-gi</a></p>
                                                        <ul class="like-answer__cmt color-text__third">
                                                            <li><i class="fa fa-thumbs-up" aria-hidden="true"></i> 5</li>
                                                            <li><i class="fa fa-reply" aria-hidden="true"></i> Trả lời 2</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="items-cmt__new">
                                    <div class="intro-cmt__new">
                                        <img src="theme/assets/images/avatar-news-cmt-1.png" alt="">
                                        <div class="text-cmt__new">
                                            <div class="top-text__cmt">
                                                <div class="name-day__up">
                                                    <p class="title-rb__bold">Lưu Minh Tâm</p>
                                                    <p>2 ngày trước</p>
                                                </div>
                                            </div>
                                            <p class="color-text__third">Cho e hỏi là giấy khai sinh bản sao của e bị mất. Vậy e nộp bản photo của bản chính đc hk ạ</p>
                                            <ul class="like-answer__cmt color-text__third">
                                                <li><i class="fa fa-thumbs-up" aria-hidden="true"></i> 5</li>
                                                <li><i class="fa fa-reply" aria-hidden="true"></i> Trả lời 2</li>
                                            </ul>
                                            <div class="items-cmt__new">
                                                <div class="intro-cmt__new">
                                                    <img src="theme/assets/images/avatar-news-cmt-2.png" alt="">
                                                    <div class="text-cmt__new">
                                                        <div class="top-text__cmt">
                                                            <div class="name-day__up">
                                                                <p class="title-rb__bold">KTS </p>
                                                                <p>2 ngày trước</p>
                                                            </div>
                                                            <p class="answer-cmt title-rb__bold color-text__third"><i class="fa fa-share" aria-hidden="true"></i> Lưu Minh Tâm </p>
                                                        </div>
                                                        <p class="color-text__third">Chào bạn, bạn lên phòng GD&ĐT của địa phương hoặc đến trường THPT để xin cấp lại giấy nhé. Giấy này bạn có thể bổ sung trong vòng 1 tháng kể từ ngày nhập học bạn nhé.</p>
                                                        <br>
                                                        <p><a href="#" title="">https://kenhtuyensinh.vn/ho-so-nhap-hoc-dai-hoc-2022-can-chuan-bi-nhung-gi</a></p>
                                                        <ul class="like-answer__cmt color-text__third">
                                                            <li><i class="fa fa-thumbs-up" aria-hidden="true"></i> 5</li>
                                                            <li><i class="fa fa-reply" aria-hidden="true"></i> Trả lời 2</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <p title="" class="btn-see__more btn-see__cmt">XEM THÊM BÌNH LUẬN...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="search-new__sidebar mb-30s">
                        <form>
                            <img src="theme/assets/images/search-new.png" alt="">
                            <input type="text" name="" placeholder="Tìm kiếm">
                        </form>
                    </div>
                    <div class="new-hot__sidebar mb-20s">
                        <h2 class="title-hara color-blues fs-38s mb-20s">Bài viết nổi bật</h2>
                        <div class="list-hot__sidebar">
                            <div class="items-new__page">
                                <div class="img-new__page">
                                    <a href="tintucchitiet.php" title="">
                                        <img src="theme/assets/images/img-new-pages.png" alt="">
                                    </a>
                                </div>
                                <div class="intros-new__page">
                                    <ul class="introduction-new__item">
                                        <li>
                                            <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                        </li>
                                        <li>
                                            <p class="color-text__third">06.07.2022</p>
                                        </li>
                                    </ul>
                                    <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                    <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                    <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category-new__sidebar">
                        <h2 class="title-hara color-blues fs-38s mb-20s">Danh mục bài viết</h2>
                        <ul class="list-category__sidebar">
                            <li><a href="tintuc.php" title="">Bảng tin Onemore</a></li>
                            <li><a href="tintuc.php" title="">BRANDING</a></li>
                            <li><a href="tintuc.php" title="">Social Media</a></li>
                            <li><a href="tintuc.php" title="">Sự Kiện</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="list-new__pages mb-25s">
        <div class="container">
            <h2 class="title-hara fs-31s mb-40s color-blues">Bài viết liên quan</h2>
            <div class="row gutter-20">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>