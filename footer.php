<footer>
    <section class="footer">
        <div class="top-footer">
            <div class="container">
                <form>
                    <div class="row gutter-40">
                        <div class="col-lg-5">
                            <p class="titles-form__footer title-rb__bold fs-17s">Đăng ký nhận nội dung mới nhất từ Onemore Branding</p>
                        </div>
                        <div class="col-lg-7">
                            <div class="group-form__footer">
                                <div class="row gutter-20">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Tên</label>
                                        <input type="text" name="" class="" placeholder="">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <label>Email</label>
                                        <input type="text" name="" class="" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="group-btn__footer">
                                <button class="btn-blue__alls">Đăng ký nhận thông tin</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="center-footer">
            <div class="container">
                <div class="top-center__footer mb-40s">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="logo-footer">
                                <a href="#" title="">
                                    <img src="theme/assets/images/logo-footer.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="top-contact__footer">
                                <img src="theme/assets/images/img-icon-contact-1.png" alt="">
                                <div class="info-mail__contact">
                                    <h3 class="fs-15s title-rb__bold color-blues">Hotline</h3>
                                    <p><a href="tel:0964747979" title="">0964 747 979</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="top-contact__footer">
                                <img src="theme/assets/images/img-icon-contact-2.png" alt="">
                                <div class="info-mail__contact">
                                    <h3 class="fs-15s title-rb__bold color-blues">Email</h3>
                                    <p><a href="mailto:contact@onemore.vn" title="">contact@onemore.vn</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-6 col-12">
                        <div class="info-footers">
                            <h3 class="title-rb__bold titles-transform__alls fs-17s mb-25s color-blues">CÔNG TY CỔ PHẦN TRUYỀN THÔNG & CÔNG NGHỆ ONEMORE</h3>
                            <div class="info-footers__items mb-15s">
                                <p class="title-rb__bold fs-15s color-blues mb-10s">Văn phòng Miền Trung</p>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 264 Nguyễn Tri Phương, Quận Thanh Khê, Thành Phố Đà Nẵng</p>
                            </div>
                            <div class="info-footers__items">
                                <p class="title-rb__bold fs-15s color-blues mb-10s">Văn phòng Miền Nam</p>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 264 Nguyễn Tri Phương, Quận Thanh Khê, Thành Phố Đà Nẵng</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-6">
                        <div class="info-footers">
                            <h3 class="title-rb__bold titles-transform__alls fs-17s mb-25s color-blues">CHÍNH SÁCH</h3>
                            <ul class="list-info__footer">
                                <li><a href="#" title="">Chính sách thanh toán</a></li>
                                <li><a href="#" title="">Chính sách bảo mật</a></li>
                                <li><a href="#" title="">Điều khoản sử dụng</a></li>
                                <li><a href="#" title="">Chính sách liên kết</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-6">
                        <div class="info-footers">
                            <div class="img-certification__footers">
                                <img src="theme/assets/images/img-footers-certification-1.png">
                                <img src="theme/assets/images/img-footers-certification-2.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="info-footers">
                            <h3 class="title-rb__bold titles-transform__alls fs-17s mb-25s color-blues">KẾT NỐI VỚI CHÚNG TÔI</h3>
                            <ul class="list-app__footer">
                                <li><a href="https://www.facebook.com/onemore.jsc" title=""><img src="theme/assets/images/app-footer-1.svg" alt=""></a></li>
                                <li><a href="https://www.youtube.com/@onemoremediadanang" title=""><img src="theme/assets/images/app-footer-2.svg" alt=""></a></li>
                                <li><a href="https://www.tiktok.com/@onemore_channel" title=""><img src="theme/assets/images/app-footer-3.svg" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-footer-4.svg" alt=""></a></li>
                                <li><a href="#" title=""><img src="theme/assets/images/app-footer-5.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="container">
                <p>Copyright 2022 © ONEMORE – Hệ sinh thái truyền thông thương hiệu và chuyển đổi số</p>
                <p><a href="#" title="">Chính sách bảo mật</a></p>
            </div>
        </div>
    </section>
    <div class="modal modal-contact fade" id="modal-contact__all" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body">
                    <div class="contact-main">
                        <div class="text-top__main titles-center__alls mb-35s">
                            <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                        </div>
                        <form class="mb-30s">
                            <div class="row gutter-20">
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                                </div>
                                <div class="col-lg-12">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                                </div>
                                <div class="col-lg-12">
                                    <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Branding
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Marcom
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                                    <div class="check-contact__alls">
                                        <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Giải pháp marketing tổng thể
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                                </div>
                                <div class="col-lg-6">
                                    <div class="up-file__prd">
                                        <input type="file" name="" class="input-files">
                                        <p class="btn-Choose__file">
                                            Gửi file đính kèm
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                                </div>
                            </div>
                            <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
                        </form>
                        <ul class="info-contact__main fs-19s">
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="theme/assets/js/jquery-3.4.1.min.js" defer></script>
<script src="theme/assets/js/moment.min.js" defer></script>
<script src="theme/assets/js/wow.min.js" defer></script>
<script src="theme/assets/js/jquery.fancybox.min.js" defer></script>
<script src="theme/assets/js/bootstrap.min.js" defer></script>
<script src="https://cdn.ckeditor.com/ckeditor5/35.3.2/classic/ckeditor.js"></script>
<script src="theme/assets/js/swiper.min.js" defer></script>
<script src="theme/assets/js/script.js" defer></script>
</body>

</html>