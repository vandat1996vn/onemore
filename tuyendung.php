<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-recruit-pages-1.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Phát triển sự nghiệp của bạn tại Công ty hàng đầu về thương hiệu</h2>
                <p class="color-text__third"> Thức thách bản thân và học hỏi kinh nghiệm với hàng trăm dự án hàng năm </p>
            </div>
        </div>
    </section>
    <section class="tip-recruit__pages mb-80s">
        <div class="container">
            <h2 class="title-hara fs-31s mb-50s color-blues">Con người luôn là trung tâm trong việc tạo ra sự khác biệt của tổ chức. Onemore tự hào với đội ngũ nhân sự năng động, sáng tạo luôn dốc hết đam mê vào mọi công việc mình làm nhằm mang lại những giá trị tốt nhất cho khách hàng.</h2>
            <a href="#content-list__recruit" class="btn-to__form btn-blue__alls titles-transform__alls">ỨNG TUYỂN NGAY</a>
        </div>
    </section>
    <section class="list-recruit__pages mb-80s" id="content-list__recruit">
        <div class="container">
            <h2 class="title-hara color-blues fs-38s titles-center__alls mb-30s">Các vị trí đang tuyển dụng</h2>
            <div class="row gutter-20 mb-60s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="pagenigation">
                <a href="#" title="" class="page-items">1</a>
                <a href="#" title="" class="page-items"> 2 </a>
                <a href="#" title="" class="page-items active"> 3 </a>
                <a href="#" title="" class="page-items"> ... </a>
                <a href="#" title="" class="page-items"> 10 </a>
            </div>
        </div>
    </section>
    <section class="quick-application mb-80s">
        <div class="container">
            <h2 class="title-hara color-blues fs-31s mb-30s">Nộp đơn ứng tuyển</h2>
            <form>
                <div class="row gutter-20">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Email*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Vị trí ứng tuyển*">
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung mô tả"></textarea>
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Tải lên CV của bạn ( tối đa 5MB
                            </p>
                            <img src="theme/assets/images/img-icons-upload.png">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn-blue__alls titles-transform__alls">ỨNG TUYỂN NGAY</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="work-environment__pages mb-60s">
        <div class="container">
            <h2 class="title-hara fs-31s mb-40s color-blues">Môi trường làm việc tại Onemore</h2>
            <div class="row gutter-20">
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>