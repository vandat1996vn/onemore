<?php @include('header.php'); ?>
<main>
    <section class="text-quocte__new mb-30s">
        <div class="container">
            <h2 class="title-hara fs-38s mb-20s color-blues">Chúng tôi chia sẻ kiến thức và kinh nghiệm làm việc để giúp bạn phát triển</h2>
        </div>
    </section>
    <section class="container">
        <div class="content-new__pages">
            <h2 class="title-rb__bold fs-20s mb-25s">Danh mục phổ biến</h2>
            <ul class="category-tag__new mb-20s">
                <li><a href="#" title="" class="active">Bảng tin Onemore</a></li>
                <li><a href="#" title="">Branding</a></li>
                <li><a href="#" title="">Social Media</a></li>
                <li><a href="#" title="">Sự Kiện</a></li>
                <li><a href="#" title="">Chiến lược</a></li>
                <li><a href="#" title="">Công nghệ</a></li>
            </ul>
            <div class="row gutter-40">
                <div class="col-lg-8">
                    <div class="new-hot__pages mb-25s">
                        <div class="items-new__page">
                            <div class="img-new__page">
                                <a href="#" title="">
                                    <img src="theme/assets/images/img-new-pages.png" alt="">
                                </a>
                            </div>
                            <div class="intros-new__page">
                                <ul class="introduction-new__item">
                                    <li>
                                        <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                    </li>
                                    <li>
                                        <p class="color-text__third">06.07.2022</p>
                                    </li>
                                </ul>
                                <h3><a href="#" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                <a href="#" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="list-new__pages">
                        <h2 class="title-hara fs-31s mb-40s color-blues">Tất cả bài viêt</h2>
                        <div class="row gutter-20 mb-60s">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="items-new__page">
                                    <div class="img-new__page">
                                        <a href="tintucchitiet.php" title="">
                                            <img src="theme/assets/images/img-new-pages.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-new__page">
                                        <ul class="introduction-new__item">
                                            <li>
                                                <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                            </li>
                                            <li>
                                                <p class="color-text__third">06.07.2022</p>
                                            </li>
                                        </ul>
                                        <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                                        <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                                        <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagenigation mb-80s">
                        <a href="#" title="" class="page-items">1</a>
                        <a href="#" title="" class="page-items"> 2 </a>
                        <a href="#" title="" class="page-items active"> 3 </a>
                        <a href="#" title="" class="page-items"> ... </a>
                        <a href="#" title="" class="page-items"> 10 </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="search-new__sidebar mb-30s">
                        <form>
                            <img src="theme/assets/images/search-new.png" alt="">
                            <input type="text" name="" placeholder="Tìm kiếm">
                        </form>
                    </div>
                    <div class="follow-apps__new mb-30s">
                        <h2 class="title-hara fs-30s mb-20s color-blues">Follow Onemore tại</h2>
                        <ul class="list-follow__apps">
                            <li><a href="#" title=""><img src="theme/assets/images/app-follow-1.png" alt=""></a></li>
                            <li><a href="#" title=""><img src="theme/assets/images/app-follow-2.png" alt=""></a></li>
                            <li><a href="#" title=""><img src="theme/assets/images/app-follow-3.png" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="new-relate__sidebar mb-30s">
                        <h2 class="title-hara fs-30s mb-20s color-blues">Bài viết liên quan</h2>
                        <div class="list-relate__sidebar">
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                            <div class="items-relate__sidebar">
                                <p class="title-rb__bold color-blues mb-10s">Social Media</p>
                                <h3><a href="tintucchitiet.php" title="" class="title-relate__sidebar fs-18s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>