<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-7.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara fs-38s mb-20s">Tư vấn thương hiệu</h2>
                <p> Dịch vụ tư vấn và cung cấp chiến lược toàn diện, phù hợp với mô hình kinh doanh để kiến tạo nền tảng phát triển vững mạnh cho doanh nghiệp. </p>
                <a href="#form-quote-main" title="" class="btn-to__form btn-blue__alls  btn-to__form titles-transform__alls">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> <span class="color-red">I. </span>Tư vấn chiến lược thương hiệu</h2>
                <p>Chiến lược thương hiệu là một bản kế hoạch dài hạn được lập ra nhằm xây dựng thương hiệu với nhiệm vụ xây dựng nhận thức về thương hiệu trong tâm trí khách hàng nhằm đạt được các mục tiêu cụ thể của doanh nghiệp.</p>
            </div>
            <div class="list-advise__trademark mb-80s">
                <div class="text-top__main titles-center__alls mb-50s">
                    <h2 class="title-hara color-blues fs-38s mb-10s">Tư vấn chiến lược thương hiệu</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">01</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-1.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Nghiên cứu</h3>
                                <p>Thực hiện nghiên cứu 4C, tạo cơ sở vững chắc cho việc xây dựng hình ảnh thương hiệu:</p>
                                <ul class="list-manufacturing__videos">
                                    <li>Category: Ngành hàng</li>
                                    <li>Company: Doanh nghiệp</li>
                                    <li>Competitor: Đối thủ</li>
                                    <li>Consumer: Người tiêu dùng</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">02</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-2.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Tư vấn lãnh đạo</h3>
                                <p>Tư vấn chiến lược xây dựng thương hiệu cho lãnh đạo cấp cao của doanh nghiệp.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">03</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-3.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Soạn thảo chiến lược</h3>
                                <p>Phối hợp với doanh nghiệp thiết kế bản chiến lược với:</p>
                                <ul class="list-manufacturing__videos">
                                    <li>Đề xuất phương án cho từng hạng mục</li>
                                    <li>Trình bày chiến lược</li>
                                    <li>Phản hồi và Hiệu chỉnh</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">04</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-4.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Quy chuẩn hóa</h3>
                                <p>Quy chuẩn hóa chiến lược thương hiệu bằng:</p>
                                <ul class="list-manufacturing__videos">
                                    <li>Văn bản</li>
                                    <li>Tài liệu hướng dẫn</li>
                                    <li>Competitor: Đối thủ</li>
                                    <li>Quy định, quy chế</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">05</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-5.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Đào tạo đội ngũ</h3>
                                <p>Tổ chức đào tạo và huấn luyện cho đội ngũ nhân sự về chiến lược phát triển thương hiệu</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">06</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-6.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Thực thi chiến lược</h3>
                                <p>Đưa chiến lược thương hiệu vào thực tế với:</p>
                                <ul class="list-manufacturing__videos">
                                    <li>Bộ nhận diện thương hiệu</li>
                                    <li>Ra mắt thương hiệu </li>
                                    <li>Truyền thông thương hiệu</li>
                                    <li>Đo lường, đánh giá và tối ưu hiệu quả</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="benefit-design__trademark mb-115s">
                <div class="row gutter-40">
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <h2 class="title-hara color-blues fs-38s mb-30s">Ưu điểm của chiến lược thương hiệu</h2>
                            <div class="row gutter-40">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Định hình chiến lược </h3>
                                        <p>Xây dựng định hướng và phương pháp xây dựng thương hiệu tổng thể.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Nâng tầm thương hiệu</h3>
                                        <p>Chiến lược thương hiệu đúng sẽ đưa doanh nghiệp đạt tầm cao mới nhanh hơn.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tối ưu chi phí</h3>
                                        <p>Tập trung nguồn lực vào mục tiêu đã hoạch định trong chiến lược để tránh lãng phí tài nguyên.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-advise-trademark-1.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-115s">
        <div class="top-design__catalouge mb-80s">
            <div class="container">
                <div class="text-top__main titles-center__alls mb-80s">
                    <h2 class="title-hara color-blues fs-38s mb-10s"> <span class="color-red">II. </span>Tư vấn ra mắt thương hiệu</h2>
                    <p>Ra mắt thương hiệu là một sự kiện đặc biệt, đòi hỏi doanh nghiệp cần có sự chuẩn bị kỹ lưỡng, có chiến lược rõ ràng và lên kế hoạch chi tiết để đảm bảo thương hiệu nhận được sự chấp nhận và ủng hộ của công chúng. </p>
                </div>
                <div class="list-element__trademark mb-115s">
                    <h2 class="title-hara titles-center__alls color-blues fs-38s mb-50s"> Dịch vụ tư vấn thương hiệu của Onemore bao gồm</h2>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-1.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">01</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Nghiên cứu thị trường</h3>
                                    <p>Xác định bối cảnh cạnh tranh, nghiên cứu mô hình SWOT, điểm mạnh, điểm yếu, cơ hội và thách thức của thương hiệu trước khi ra mắt.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-2.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">02</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Đánh giá thương hiệu</h3>
                                    <p>Đo lường sức khỏe của thương hiệu, mức độ nhận biết của thương hiệu thông qua các công cụ nghiên cứu bài bản.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-3.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">03</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Định vị thương hiệu</h3>
                                    <p>Xây dựng hình ảnh thương hiệu khác biệt trong lòng khách hàng nhằm xây dựng định vị thương hiệu độc đáo, phù hợp với mục tiêu và định hướng của doanh nghiệp.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-4.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">04</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Cấu trúc thương hiệu</h3>
                                    <p>Tư vấn xây dựng mô hình hoạt động hiệu quả, tối ưu nguồn lực và hoạt động marketing để tiết kiệm tối đa chi phí.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-5.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">05</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Xây dựng chiến lược truyền thông</h3>
                                    <p>Tư vấn chiến lược truyền thông hiệu quả giúp thương hiệu tiếp cận khách hàng mục tiêu từ khi ra mắt.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-50s">
                                <img src="theme/assets/images/img-element-trademark-6.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <p class="number-element__trademark mb-25s title-rb__bold">06</p>
                                    <h3 class="title-rb__bold color-blues fs-18s mb-10s">Thiết kế bộ nhận diện thương hiệu</h3>
                                    <p>Xây dựng hệ thống nhận diện thương hiệu tổng thể, đồng bộ và nhất quán, bao gồm: logo, slogan, hồ sơ năng lực, sales kit,... </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mb-30s">
            <div class="benefit-design__trademark mb-30s">
                <div class="row gutter-40">
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-element-trademark-advantages.png">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <h2 class="title-hara color-blues fs-38s mb-30s">Ưu điểm của dịch vụ ra mắt thương hiệu</h2>
                            <div class="row gutter-40">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Định hình chiến lược </h3>
                                        <p>Xây dựng định hướng và phương pháp xây dựng thương hiệu tổng thể.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Nâng tầm thương hiệu</h3>
                                        <p>Chiến lược thương hiệu đúng sẽ đưa doanh nghiệp đạt tầm cao mới nhanh hơn.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Thu hút khách hàng trung thành</h3>
                                        <p>Tập trung nguồn lực vào mục tiêu đã hoạch định trong chiến lược để tránh lãng phí tài nguyên.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tối ưu hiệu quả</h3>
                                        <p>Tập trung nguồn lực vào mục tiêu đã hoạch định trong chiến lược để tránh lãng phí tài nguyên.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> <span class="color-red">III. </span>Tư vấn tái định vị thương hiệu</h2>
                <p>Tái định vị thương hiệu là hoạt động giúp doanh nghiệp định vị lại vị trí thương hiệu trong tâm trí khách hàng hay nói cách khác là làm mới hình ảnh, tạo một sức sống mới cho thương hiệu nhằm đáp ứng được sự thay đổi của thị trường hay đạt được mục tiêu kinh doanh của doanh nghiệp.</p>
            </div>
            <div class="list-advise__trademark mb-80s">
                <div class="text-top__main titles-center__alls mb-50s">
                    <h2 class="title-hara color-blues fs-38s mb-10s">Quy trình tư vấn chiến lược thương hiệu</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">01</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-1.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Nghiên cứu thị trường</h3>
                                <p>Thực hiện nghiên cứu để xác định bối cảnh thị trường, cơ hội và thách thức cho doanh nghiệp.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">02</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-7.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Nghiên cứu đối thủ</h3>
                                <p>Đánh giá mức độ nhận biết của khách hàng về thương hiệu và năng lực của đối thủ để đưa ra chiến lược hợp lý.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">03</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-8.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Nghiên cứu thương hiệu</h3>
                                <p>Nghiên cứu thực trạng và lợi thế cạnh tranh của thương hiệu và kiểm tra sự phù hợp của lợi thế cạnh tranh với khách hàng và thị trường.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">04</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-9.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Hoạch định chiến lược</h3>
                                <p>Lựa chọn chiến lược phù hợp với lợi thế của thương hiệu và bối cảnh thị trường.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">05</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-10.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Tư vấn truyền thông</h3>
                                <p>Đề xuất chiến lược truyền thông để phục vụ chiến lược tái định vị thương hiệu đã hoạch định.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <p class="number-standard__advise">06</p>
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-advise-standard-11.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-30s">Thực thi chiến lược</h3>
                                <p>Sáng tạo nội dung, hình ảnh, nguyên liệu truyền thông cho chiến lược tái định vị. Đồng thời, theo dõi, đánh giá và tối ưu các chỉ số nhằm đạt được mục tiêu.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="benefit-design__trademark mb-115s">
                <div class="row gutter-40">
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <h2 class="title-hara color-blues fs-38s mb-30s">Lợi ích của dịch vụ tái định vị thương hiệu</h2>
                            <div class="row gutter-40">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Phát triển bền vững </h3>
                                        <p>Tái định vị thương hiệu giúp doanh nghiệp mở rộng thị phần và tăng trưởng doanh thu hiệu quả.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Thu hút khách hàng trung thành</h3>
                                        <p>Chiến dịch tái định vị thương hiệu thành công sẽ giúp khách hàng tin tưởng vào sản phẩm và dịch vụ của thương hiệu, từ đó dễ dàng ra quyết định mua hàng.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Đồng bộ chiến lược</h3>
                                        <p>Tạo nên sự đồng nhất giữa chiến lược của thương hiệu và chiến lược kinh doanh của doanh nghiệp.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="items-design__trademark mb-30s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Kết nối khách hàng</h3>
                                        <p>Chiến lược tái định vị thương hiệu sẽ tạo sợi dây liên kết với khách hàng, tăng sự trung thành.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-advise-trademark-2.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>