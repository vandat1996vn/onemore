<?php @include('header.php'); ?>
<main>
    <section class="slide-mains mb-70s">
        <div class="banner-mains swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="items-banner__mains">
                        <div class="container">
                            <div class="row gutter-140">
                                <div class="col-lg-6">
                                    <div class="intros-banner__main">
                                        <h2 class="title-hara color-blues fs-40s mb-10s">One More Branding</h2>
                                        <h3 class="title-hara mb-35s color-text__second fs-31s">Giải pháp xây dựng thương hiệu toàn diện</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                                        <button class="btn-blue__alls titles-transform__alls">NHẬN TƯ VẤN</button>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="img-banner__main">
                                        <img src="theme/assets/images/img-slide-main.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="items-banner__mains">
                        <div class="container">
                            <div class="row gutter-140">
                                <div class="col-lg-6">
                                    <div class="intros-banner__main">
                                        <h2 class="title-hara color-blues fs-40s mb-10s">One More Branding</h2>
                                        <h3 class="title-hara mb-35s color-text__second fs-31s">Giải pháp xây dựng thương hiệu toàn diện</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                                        <button class="btn-blue__alls titles-transform__alls">NHẬN TƯ VẤN</button>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="img-banner__main">
                                        <img src="theme/assets/images/img-slide-main.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>
    <section class="about-mains">
        <div class="container">
            <div class="row gutter-120">
                <div class="col-lg-6">
                    <div class="intros-about__main">
                        <h2 class="title-hara color-blues fs-38s mb-30s">Giới thiệu về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        <a class="btn-red__alls" href="#">Về onemore</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="img-about__main">
                        <img src="theme/assets/images/about-mains.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vision-main mb-115s">
        <div class="container">
            <h2 class="title-hara titles-center__alls fs-38s mb-35s">Tầm nhìn sứ mệnh của chúng tôi</h2>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="item-vision__main">
                        <img src="theme/assets/images/img-vision-main-1.png" alt="">
                        <div class="intro-vison__item">
                            <h3 class="title-hara fs-25s mb-10s">Mục tiêu</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="item-vision__main">
                        <img src="theme/assets/images/img-vision-main-2.png" alt="">
                        <div class="intro-vison__item">
                            <h3 class="title-hara fs-25s mb-10s">Mục tiêu</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="reason-mains mb-190s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Lý do nên chọn Onemore</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="list-parameter__reason">
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">15000+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">86+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">10+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
            </div>
        </div>
    </section>
    <section class="sevice-main mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Dịch vụ chính</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="row mb-40s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-sevice__main">
                        <div class="img-item__sevice mb-20s">
                            <a href="tuvanthuonghieu.php" title="">
                                <img src="theme/assets/images/img-item-sevice-1.png" alt="">
                            </a>
                        </div>
                        <div class="content-item__sevice">
                            <h3><a href="tuvanthuonghieu.php" class="title-item__sevice title-rb__bold fs-21s mb-35s">Branding</a></h3>
                            <ul class="info-item__sevice">
                                <li><a href="tuvanthuonghieu.php" title="">Tư vấn thương hiệu</a></li>
                                <li><a href="thietkenhandienthuonghieu.php" title="">Thiết kế nhận diện thương hiệu</a></li>
                                <li><a href="thietkeanphamtruyenthong.php" title="">Thiết kế ấn phẩm marketing</a></li>
                            </ul>
                            <a class="btn-red__alls" href="tuvanthuonghieu.php">Xem thêm</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-sevice__main">
                        <div class="img-item__sevice mb-20s">
                            <a href="maketingtruyenthong.php" title="">
                                <img src="theme/assets/images/img-item-sevice-2.png" alt="">
                            </a>
                        </div>
                        <div class="content-item__sevice">
                            <h3><a href="maketingtruyenthong.php" class="title-item__sevice title-rb__bold fs-21s mb-35s">Marcom</a></h3>
                            <ul class="info-item__sevice">
                                <li><a href="maketingtruyenthong.php" title="">Truyền thông</a></li>
                                <li><a href="digitalmarketing.php" title="">Digital Marketing</a></li>
                                <li><a href="production.php" title="">Production/ Media</a></li>
                            </ul>
                            <a class="btn-red__alls" href="maketingtruyenthong.php">Xem thêm</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-sevice__main">
                        <div class="img-item__sevice mb-20s">
                            <a href="maketingtongthe.php" title="">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="content-item__sevice">
                            <h3><a href="maketingtongthe.php" class="title-item__sevice title-rb__bold fs-21s mb-35s">Giải pháp marketing tổng thể</a></h3>
                            <a class="btn-red__alls" href="maketingtongthe.php">Xem thêm</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="register-sevice__main">
                <h3 class="title-hara color-blues fs-25s mb-10s">Đăng ký nhận báo giá</h3>
                <p class="mb-20s">Lorem ipsum dolor sit amet, consectetuzer adipiscing elit</p>
                <button data-toggle="modal" data-target="#modal-contact__all" title="" class="btn-blue__trans btn-to__form titles-transform__alls">ĐĂNG KÝ DỊCH VỤ</button>
            </div>
        </div>
    </section>
    <section class="project-main mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Dự án tiêu biểu đã thực hiện</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="tag-prj__alls">
                <ul class="nav tag-prj__mains mb-70s" id="myTab" role="tablist">
                    <li>
                        <a title="" class=" active" id="prj-mains-tag-1-tab" data-toggle="tab" href="#prj-mains-tag-1" role="tab" aria-controls="#prj-mains-tag-1" aria-selected="true"> TVC
                        </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-2-tab" data-toggle="tab" href="#prj-mains-tag-2" role="tab" aria-controls="prj-mains-tag-2" aria-selected="false"> F&B </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-3-tab" data-toggle="tab" href="#prj-mains-tag-3" role="tab" aria-controls="prj-mains-tag-3" aria-selected="false"> Beauty </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-4-tab" data-toggle="tab" href="#prj-mains-tag-4" role="tab" aria-controls="prj-mains-tag-4" aria-selected="false"> Event </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-5-tab" data-toggle="tab" href="#prj-mains-tag-5" role="tab" aria-controls="prj-mains-tag-5" aria-selected="false"> Tất cả dự án </a>
                    </li>
                </ul>
                <div class="tab-content content-prj__tag">
                    <div class="tab-pane fade show active" id="prj-mains-tag-1" role="tabpanel" aria-labelledby="prj-mains-tag-1-tab">
                        <div class="row mb-30s">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/before-video-prj-main.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#" title="" class="btn-see__more">XEM TẤT CẢ</a>
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-2" role="tabpanel" aria-labelledby="prj-mains-tag-2-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-3" role="tabpanel" aria-labelledby="prj-mains-tag-3-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-4" role="tabpanel" aria-labelledby="prj-mains-tag-4-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-5" role="tabpanel" aria-labelledby="prj-mains-tag-5-tab">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="new-main mb-115s">
        <div class="container">
            <div class="top-new__main mb-30s">
                <h2 class="title-hara color-blues fs-38s">Tin tức & kiến thức</h2>
                <a class="btn-red__alls" href="#">Xem thêm</a>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-new__page">
                        <div class="img-new__page">
                            <a href="tintucchitiet.php" title="">
                                <img src="theme/assets/images/img-new-pages.png" alt="">
                            </a>
                        </div>
                        <div class="intros-new__page">
                            <ul class="introduction-new__item">
                                <li>
                                    <p class="title-rb__bold color-blues">Bảng tin Onemore</p>
                                </li>
                                <li>
                                    <p class="color-text__third">06.07.2022</p>
                                </li>
                            </ul>
                            <h3><a href="tintucchitiet.php" title="" class="title-new__pages fs-18s mb-5s title-rb__bold">Khung hình 9:16 liệu có thể chiếm lĩnh thế giới quảng cáo?</a></h3>
                            <p>Sau khi kết thúc thời gian đăng ký xét tuyển đợt 1, từ ngày 30/07/2022 các trường đại học sẽ công bố điểm...</p>
                            <a href="tintucchitiet.php" class="see-new__page" title="">Xem thêm <img src="theme/assets/images/arrow-btn-slide.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-main mb-115s">
        <div class="container-fluid">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Khách hàng đã hợp tác cùng Onemore</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="row gutter-100">
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <div class="item-patner__main">
                        <img src="theme/assets/images/img-partner-main-1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>