<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-20s">
        <img src="theme/assets/images/img-sevice-pages-5.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Digital Marketing</h2>
                <p class="color-text__third"> Marketing tổng thể là chiến lược marketing toàn diện về mọi mặt của một doanh nghiệp, bao gồm từ bước cơ bản nhất là nghiên cứu thị trường đến giai đoạn đo lường kết quả. </p>
                <a href="#" title="" class="btn-blue__alls titles-blues__alls btn-to__form ">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="title-digital__maketing mb-80s">
        <div class="container">
            <h2 class="title-hara color-blues fs-38s">Dịch vụ viết bài chuẩn SEO</h2>
        </div>
    </section>
    <section class="design-logo__sevice mb-80s">
        <div class="container">
            <div class="benefit-design__trademark">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">Vai trò của SEO website</h2>
                <div class="row gutter-40">
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/role-seo-pages.png">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tiết kiệm chi phí</h3>
                                        <p>Thay vì bỏ một số tiền lớn cho việc quảng cáo Google Ads để website xuất hiện trên công cụ tìm kiếm, doanh nghiệp hoàn toàn có thể làm được điều đó với SEO. Nội dung ấn tượng, cung cấp đầy đủ thông tin đến người dùng và xác định từ khóa đúng với insight của người dùng sẽ giúp website đạt thứ hạng cao nhanh hơn.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tăng lượng truy cập</h3>
                                        <p>Website đạt thứ hạng cao trên công cụ tìm kiếm sẽ giúp lượt truy cập vào website tăng lên, tăng lượng traffic và đưa thông tin đến nhiều đối tượng khách hàng tiềm năng.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Đánh giá trải nghiệm người dùng</h3>
                                        <p> Những phân tích, nghiên cứu trong SEO giúp doanh nghiệp nhận biết và khắc phục những vấn đề tồn tại trên website. Từ đó nâng cao trải nghiệm và thu hút khách hàng..</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tạo độ tin cậy cho doanh nghiệp</h3>
                                        <p> Một website cung cấp những thông tin chất lượng, bổ ích với người dùng và đạt thứ hạng cao trên công cụ tìm kiếm sẽ giúp tạo thiện cảm và niềm tin từ khách hàng.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advise-maketing__pages mb-80s">
        <img src="theme/assets/images/bg-advise-maketing.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s">Quảng cáo trực tuyến</h2>
        </div>
    </section>
    <section class="design-logo__sevice manufacturing-videos__marketing mb-130s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> Sản xuất video marketing </h2>
            </div>
            <div class="list-design__catalouge list-standard__all">
                <div class="row gutter-20 justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-24.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Mở rộng tập khách hàng tiềm năng</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Lượng người dùng trên các nền tảng trực tuyến là rất lớn và không ngừng tăng lên, điều đó đồng nghĩa rằng doanh nghiệp có cơ hội tiếp cận khách hàng hiện tại, khách hàng mới và cả những người quan tâm đến doanh nghiệp hoặc tổ chức của mình.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-25.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Nhắm đến mục tiêu cụ thể</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Quảng cáo trực tuyến cho phép người dùng định hướng quảng cáo của mình đến những đối tượng cụ thể dựa vào sở thích, nhân khẩu học, thiết bị sử dụng, từ khóa tìm kiếm hay thói quen của họ.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-26.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Tỉ lệ chuyển đổi cao </h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Thói quen tìm kiếm và mua hàng trực tuyến đang ngày càng phổ biến, vì vậy nếu quảng cáo đủ thu hút sẽ tạo ra động cơ mua hàng rất lớn cho người tiêu dùng.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-27.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Thiết lập ngân sách hợp lý</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Vì có thể nhắm đối tượng mục tiêu cụ thể nên ngân sách phải chi cho quảng cáo trực tuyến cũng tiết kiệm hơn so với các hình thức truyền thống. Chưa kể doanh nghiệp chỉ cần trả tiền khi có ai đó nhấp vào quảng cáo.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-28.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Dễ quản lý và đo lường hiệu quả</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Các nền tảng đều có công cụ và cung cấp số liệu chi tiết cho mỗi quảng cáo. Vì vậy doanh nghiệp dễ dàng quản lý và đánh giá được mức độ thành công của các chiến dịch.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-80s">
        <div class="container">
            <div class="benefit-design__trademark">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">Q&A</h2>
                <div class="row gutter-40">
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Các kênh của tôi đang xây nội dung tự nhiên thì có cần quảng cáo không?</h3>
                                        <p>Mặc dù việc sản xuất và đăng tải nội dung thường xuyên lên kênh là một cách hữu hiệu để tiếp cận khách hàng, doanh nghiệp vẫn nên tính đến việc dùng quảng cáo để mở rộng phạm vi tiếp cận. Hiện nay có ngày càng nhiều nội dung được tạo ra trên các nền tảng trực tuyến, đồng nghĩa với việc doanh nghiệp càng khó hiển thị nội dung của mình cho khán giả mục tiêu. Để tiếp tục thu hút đối tượng, tiếp cận những người mới và chia sẻ các sản phẩm hoặc dịch vụ mới nhất của mình, bạn nên sử dụng quảng cáo.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tôi có thể tự quảng cáo cho các kênh của mình được không?</h3>
                                        <p>Mọi người đều có thể quảng cáo. Tuy nhiên, nếu không đủ chuyên môn thì chiến dịch sẽ khó đảm bảo được kết quả mang lại, gây lãng phí ngân sách. Do đó, doanh nghiệp nên có sự hỗ trợ của chuyên gia và đội ngũ ekip có kinh nghiệm khi thực thi để tối ưu hiệu quả quảng cáo.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Vì sao gói giải pháp quảng cáo của Onemore chỉ tập trung vào ba nền tảng là Google, Facebook và TikTok?</h3>
                                        <p> Hiện nay Google là công cụ tìm kiếm phổ biến nhất; Facebook là mạng xã hội lớn nhất và đa dạng đối tượng người dùng; TikTok là mạng xã hội xu hướng có tốc độ phát triển nhanh nhất và tập trung người dùng Gen Z và Millennial. Vì vậy Onemore muốn giúp doanh nghiệp tập trung khai thác tiềm năng của ba nền tảng này để vừa đảm bảo hiệu quả, vừa tối ưu kinh phí quảng cáo.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-qa.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="brand-partner__pages mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-3">
                    <h2 class="title-hara color-blues fs-26s">5000+ Đối tác đã tin tưởng hợp tác</h2>
                </div>
                <div class="col-lg-9">
                    <div class="sl-brand__partner swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>