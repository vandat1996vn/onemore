<!DOCTYPE html>
<html itemscope="" itemtype="http://schema.org/WebPage" lang="vi">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="theme/assets/css/animate.css">
    <link rel="stylesheet" href="theme/assets/css/font-awesome.css">
    <link rel="stylesheet" href="theme/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="theme/assets/css/swiper.min.css" />
    <link rel="stylesheet" href="theme/assets/css/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="theme/assets/css/cloudzoom.css">
    <link rel="stylesheet" href="theme/assets/css/main.css">
    <link rel="stylesheet" href="theme/assets/css/about.css">
    <link rel="stylesheet" href="theme/assets/css/sevice.css">
    <link rel="stylesheet" href="theme/assets/css/project.css">
    <link rel="stylesheet" href="theme/assets/css/contact.css">
    <link rel="stylesheet" href="theme/assets/css/recruit.css">
    <link rel="stylesheet" href="theme/assets/css/new.css">
    <link rel="stylesheet" href="theme/assets/css/reset.css">
    <link rel="stylesheet" href="theme/assets/css/responsive.css">
</head>

<body>
    <header>
        <section class="header">
            <div class="news-slogan__header">
                <div class="container">
                    <p class="title-slogan__header">Tin tức:</p>
                    <span class="titles-headers text-marquee">Tiger Arena 2022 chính thức khép lại với những khoảnh khắc “rực lửa”</span>
                    <p class="time-slogan__heager">Da Nang, ngày 11. tháng 11, năm 2022</p>
                </div>
            </div>
            <div class="bottom-headers">
                <div class="container">
                    <div class="logo-mains">
                        <a href="index.php"><img alt="" src="theme/assets/images/logo-mains.svg"></a>
                    </div>
                    <span class="button-phone btn_sp_menu"><img src="theme/assets/images/img-btn-mobiles.png" alt=""></span>
                    <div class="rights-bottoms__headers">
                        <div class="menu">
                            <div class="content-menus menu-desktop">
                                <div class="header-menu-mobile">
                                    <a href="index.php" title="" class="logo-menu__mobile"> <img alt="" alt="" src="theme/assets/images/logo-mains.png" class="img-fluid"></a>
                                    <span class="close-menu-btn">x</span>
                                </div>
                                <ul>
                                    <li><a href="index.php" title="" class="">Trang chủ</a></li>
                                    <li>
                                        <a href="hosonangluc.php" title="">Về Onemore</a>
                                        <ul>
                                            <li><a href="vechungtoi.php" title="">Chúng tôi là ai ?</a></li>
                                            <li><a href="hosonangluc.php" title="">Hồ sơ năng lực</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" title="">Dịch vụ</a>
                                        <ul>
                                            <li>
                                                <a href="#" title="">Branding</a>
                                                <ul>
                                                    <li>Giải pháp sáng tạo xây dựng tổng thể Giải pháp sáng tạo xây dựng tổng thểGiải pháp sáng tạo xây dựng </li>
                                                    <li><a href="tuvanthuonghieu.php" title="">Tư vấn thương hiệu</a></li>
                                                    <li><a href="thietkenhandienthuonghieu.php" title="">Thiết kế nhận diện thương hiệu</a></li>
                                                    <li><a href="thietkeanphamtruyenthong.php" title="">Thiết kế ấn phẩm marketing</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#" title="">Marcom</a>
                                                <ul>
                                                    <li>Dịch vụ tư vấn và cung cấp chiến lược toàn diện, phù hợp với mô hình kinh doanh để kiến tạo nền tảng phát triển vững mạnh cho doanh nghiệp. </li>
                                                    <li><a href="maketingtruyenthong.php" title="">Truyền thông </a></li>
                                                    <li><a href="digitalmarketing.php" title="">Digital Marketing</a></li>
                                                    <li><a href="production.php" title="">Production/ Media</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#" title="">Marketing tổng thể</a>
                                                <ul>
                                                    <li>Marketing tổng thể là chiến lược marketing toàn diện về mọi mặt của một doanh nghiệp, bao gồm từ bước cơ bản nhất là nghiên cứu thị trường đến giai đoạn đo lường kết quả. </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="duan.php" title="">Dự án</a></li>
                                    <li><a href="tintuc.php" title="">Tin tức</a></li>
                                    <li><a href="tuyendung.php" title="">Tuyển dụng</a></li>
                                    <li><a href="lienhe.php" title="">Liên hệ</a></li>
                                </ul>
                                <p class="btn-mobiles__menu"><i class="fa fa-phone" aria-hidden="true"></i> Hotline : <a href="tel:0933546453" title="">0933 546 453</a></p>
                            </div>
                        </div>
                        <div class="bg-over-menu"></div>
                    </div>
                </div>
            </div>
        </section>
    </header>