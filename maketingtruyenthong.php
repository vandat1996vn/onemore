<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-6.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Truyền thông</h2>
                <p class="color-text__third"> Marketing tổng thể là chiến lược marketing toàn diện về mọi mặt của một doanh nghiệp, bao gồm từ bước cơ bản nhất là nghiên cứu thị trường đến giai đoạn đo lường kết quả. </p>
                <a href="#" title="" class="btn-blue__alls titles-blues__alls btn-to__form ">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice manufacturing-videos__marketing mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Quản trị fanpage</h2>
                <p>Quản trị fanpage là các hoạt động tối ưu nội dung, chăm sóc fanpage nhằm duy trì, kiểm soát và phát triển trong một các hiệu quả và đem lại lợi ích cho doanh nghiệp.</p>
            </div>
            <div class="benefit-design__trademark mb-80s">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">Tầm quan trọng của việc quản trị fanpage</h2>
                <div class="row gutter-40">
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/role-seo-pages.png">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Kết nối thương hiệu với khách hàng</h3>
                                        <p>Chủ động tìm kiếm thông tin về sản phẩm/dịch vụ và thương hiệu gần như đã trở thành một bước quan trọng trước khi mua hàng của người tiêu dùng. Do đó việc doanh nghiệp cung cấp đầy đủ thông tin và có hoạt động tư vấn qua fanpage sẽ giúp tăng động cơ mua của khách hàng và giữ chân họ ở lại với doanh nghiệp.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tiếp cận khách hàng tiềm năng</h3>
                                        <p>Một fanpage có nội dung hấp dẫn sẽ thu hút được sự quan tâm của những đối tượng mục tiêu mà doanh nghiệp hưởng đến. Fanpage cũng có nhiều lượt truy cập thì tỉ lệ tiếp cận tập khách hàng tiềm năng căng cao.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tiết kiệm chi phí</h3>
                                        <p> Nội dung trên fanpage có khả năng truyền tải và chia sẻ thông tin nhanh chóng, không bị giới hạn không gian, thời gian. Mặt khác, hiện nay xu hướng mua hàng trực tuyến ngày càng phổ biến, doanh nghiệp hoàn toàn có thể kinh doanh trực tuyến qua fanpage mà không cần có cửa hàng vật lý. Vì vậy mà hoạt động Marketing và bán hàng trở nên tiết kiệm chi phí hơn.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tạo dấu ấn trong tâm trí khách hàng</h3>
                                        <p> Qua tông giọng, phong cách thiết kế của các nội dung được đăng tải trên fanpage, người đọc sẽ có những ấn tượng nhất định về thương hiệu. Nhờ đó mà thương hiệu dễ dàng định vị hình ảnh của mình trong tâm trí khách hàng, tăng mức độ nhận diện thương hiệu.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> Q&A </h2>
            </div>
            <div class="list-design__catalouge list-standard__all">
                <div class="row gutter-20 justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-29.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Doanh nghiệp không bán hàng trực tuyến có cần quản trị fanpage không?</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Việc quản trị fanpage nhằm cung cấp thông tin, quảng bá sản phẩm/dịch vụ, tư vấn và thu hút khách hàng tiềm năng đều cần thiết và đem lại hiệu quả cho cả hai hình thức kinh doanh trực tiếp và trực tuyến. Do đó, quản trị fanpage là hình thức marketing cần thiết cho sự phát triển của doanh nghiệp.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-30.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Tôi có thể tự quản trị fanpage cho doanh nghiệp minh được không?</h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Facebook cho phép ai cũng có thể quản trị fanpage của riêng minh. Tuy nhiên, nếu không có chuyên môn về marketing, việc quản trị fanpage khó có thể đem lại kết quả như mong muốn, gây lãng phí nguồn lực của doanh nghiệp. Vì vậy để đảm bảo hiệu quả, doanh nghiệp nên có sự hỗ trợ của đội ngũ giàu kinh nghiệm.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-31.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s"> Gói giải pháp của Onemore mang lại cho doanh nghiệp của tôi những lợi ích gì </h3>
                                <ul class="list-manufacturing__videos">
                                    <li>Những lợi ích mà doanh nghiệp nhận được khi sử dụng gói giải pháp của Onemore có thể kể đến như:</li>
                                    <li>Xây dựng nội dung theo chiến lược marketing.</li>
                                    <li>Tạo hình ảnh chuyên nghiệp, chỉnh chu cho thương hiệu.</li>
                                    <li>Định hướng dư luận theo mong muốn của doanh nghiệp.</li>
                                    <li>Đảm bảo lượng tiếp cận và mức độ phát triển của fanpage.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advise-maketing__pages mb-80s">
        <img src="theme/assets/images/bg-maketing-tiktok.png" alt="">
        <div class="container">
            <h2 class="title-hara fs-38s">Biên tập video Tiktok</h2>
        </div>
    </section>
    <section class="design-logo__sevice manufacturing-videos__marketing mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Tầm quan trọng của xu hướng TikTok</h2>
                <p>TikTok là đang một trong những mạng xã hội phổ biến và có lượng người dùng tăng trưởng nhanh nhất toàn cầu. Trên thế giới nói chung và ở Việt Nam nói riêng đã có rất nhiều chiến dịch marketing của các nhãn hàng thành công vang dội nhờ bắt kịp xu hướng này. Vì vậy, hiện nay TikTok là một kênh quảng bá sản phẩm/dịch vụ hiệu quả mà các doanh nghiệp không nên bỏ qua.</p>
            </div>
            <div class="solution-package__benefits mb-50s">
                <div class="text-top__main titles-center__alls mb-70s">
                    <h2 class="title-hara color-blues fs-38s mb-10s">Lợi ích của gói giải pháp biên tập video TikTok đối với doanh nghiệp</h2>
                    <p>Với gói giải pháp biên tập video TikTok, 1M sẽ hỗ trợ doanh nghiệp xử lý và sắp xếp hình ảnh, video có sẵn thành sản phẩm hoàn chỉnh và hấp dẫn. Qua đó giúp:</p>
                </div>
                <div class="benefit-design__brochure">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-30s">
                                <img src="theme/assets/images/img-importance-tiktok-1.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s mb-30s">Chuyên nghiệp hóa hình ảnh thương hiệu</h3>
                                    <p>Đội ngũ biên tập giàu kinh nghiệm mang đến những video chỉnh chu, tạo hình ảnh chuyên nghiệp cho sản phẩm/dịch vụ.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-30s">
                                <img src="theme/assets/images/img-importance-tiktok-2.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s mb-30s">Tiết kiệm thời gian cho khách hàng</h3>
                                    <p>Dịch vụ của Onemore hỗ trợ doanh nghiệp sản xuất lên đến 90 video/tháng.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-30s">
                                <img src="theme/assets/images/img-importance-tiktok-3.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s mb-30s">Tối ưu hóa nội dung</h3>
                                    <p>Video được biên tập một cách kỹ lưỡng, đảm bảo nội dung hấp dẫn, thu hút người xem, truyền tải rõ ràng thông điệp mà doanh nghiệp muốn gửi gắm đến khách hàng của mình.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="items-benefit__brochure mb-30s">
                                <img src="theme/assets/images/img-importance-tiktok-4.png" alt="">
                                <div class="intro-benefit__brochure">
                                    <h3 class="title-rb__bold color-blues fs-18s mb-30s">Phù hợp với Tiêu chuẩn Cộng đồng</h3>
                                    <p>Onemore đảm bảo tất cả video được biên tập đều phù hợp với chính sách Tiêu chuẩn Cộng đồng của TikTok.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="benefit-design__trademark">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-40s">Q&A</h2>
                <div class="row gutter-40">
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Gói giải pháp biên tập video TikTok dành cho ai?</h3>
                                        <p>Gói giải pháp biên tập video TikTok của Onemore phù hợp với các doanh nghiệp muốn triển khai marketing trên TikTok nhưng chưa có nhân lực biên tập video chuyên nghiệp, hoặc các đơn vị cần sản xuất số lượng lớn video hàng tháng.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Onemore có hỗ trợ doanh nghiệp lên kịch bản và quay video không?</h3>
                                        <p>Với gói giải pháp biên tập video TikTok, Onemore chỉ hỗ trợ tư vấn về mặt nội dung và sản xuất video, không trực tiếp tham gia các quá trình này. Nếu khách hàng có nhu cầu về dịch vụ sản xuất video trọn gói, vui lòng tham khảo “Gói giải pháp sản xuất video marketing” của Onemore.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Doanh nghiệp hoạt động trong lĩnh vực nào nên marketing trên kênh TikTok?</h3>
                                        <p>Hiện tại, TikTok đang đẩy mạnh các nội dung liên quan đến ngành hàng thời trang, làm đẹp, thực phẩm, du lịch, game và các ngành bán lẻ. Đây là cơ hội lớn dành cho các doanh nghiệp đang hoạt động trong các lĩnh vực trên muốn triển khai marketing trên kênh truyền thông này.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/img-maketing-tiktok.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="brand-partner__pages mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-3">
                    <h2 class="title-hara color-blues fs-26s">5000+ Đối tác đã tin tưởng hợp tác</h2>
                </div>
                <div class="col-lg-9">
                    <div class="sl-brand__partner swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="prj-brand__pages mb-80s">
        <div class="container">
            <h2 class="title-hara titles-center__alls color-blues fs-38s mb-20s">Các dự an Production/ Media đã thực hiện</h2>
            <div class="row mb-20s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" title="" class="btn-see__more">XEM thêm</a>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>