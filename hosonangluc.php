<?php @include('header.php'); ?>
<main>
    <section class="banner-capacity banner-capacity__left">
        <div class="container">
            <div class="intros-banner__capacity">
                <h2 class="title-hara color-blues fs-38s mb-10s">Đồng hành cùng 10000</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
            </div>
        </div>
        <div class="img-banner__capacity">
            <img src="theme//assets/images/banner-capacity-1.png" alt="">
        </div>
    </section>
    <section class="banner-capacity banner-capacity__right mb-150s">
        <div class="container">
            <div class="intros-banner__capacity">
                <h2 class="title-hara color-blues fs-38s mb-10s">Giới thiệu về Onemore</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
        </div>
        <div class="img-banner__capacity">
            <img src="theme//assets/images/banner-capacity-2.png" alt="">
        </div>
    </section>
    <section class="slide-capacity__file mb-30s">
        <div class="container">
            <h2 class="title-hara color-blues fs-38s mb-10s">Hồ sơ năng lực Onemore</h2>
            <div class="sl-capacity__file swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
            <div class="group-btns__showss">
                <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
            </div>
        </div>
    </section>
    <section class="slide-capacity__file mb-115s">
        <div class="container">
            <h2 class="title-hara color-blues fs-38s mb-10s">Portfolio</h2>
            <div class="sl-capacity__file swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="items-capacity__file">
                            <div class="img-capacity__file">
                                <img src="theme/assets/images/img-capacity-file.png" alt="">
                            </div>
                            <div class="intros-capacity__file">
                                <h3 class="title-hara color-penta fs-25s mb-40s">Profile Onemore Company</h3>
                                <button class="btn-dowload__alls">Download <img src="theme/assets/images/icons-dowload-files.png"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
            <div class="group-btns__showss">
                <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
            </div>
        </div>
    </section>
    <section class="reason-mains mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Lý do nên chọn Onemore</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="list-parameter__reason">
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">15000+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">86+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">10+</h3>
                    <p class="fs-21s">Lorem ipsum</p>
                </div>
            </div>
        </div>
    </section>
    <section class="banner-map__capacity mb-115s">
        <img src="theme/assets/images/banner-map-abour.png" alt="">
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>