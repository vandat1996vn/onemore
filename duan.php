<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages-6.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara color-blues fs-38s mb-20s">Một số dự án Onemore đã thực hiện</h2>
                <p class="color-text__third mb-50s"> Mỗi dự án được hoàn thành là tâm huyết sáng tạo của Onemore. Chúng tôi tự hào nhận được sự tin tưởng và góp phần thành công cho những dự án của doanh nghiệp. </p>
            </div>
        </div>
    </section>
    <section class="project-main mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Dự án tiêu biểu đã thực hiện</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <div class="tag-prj__alls">
                <ul class="nav tag-prj__mains mb-70s" id="myTab" role="tablist">
                    <li>
                        <a title="" class=" active" id="prj-mains-tag-1-tab" data-toggle="tab" href="#prj-mains-tag-1" role="tab" aria-controls="#prj-mains-tag-1" aria-selected="true"> Tất cả dự án
                        </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-2-tab" data-toggle="tab" href="#prj-mains-tag-2" role="tab" aria-controls="prj-mains-tag-2" aria-selected="false"> F&B </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-3-tab" data-toggle="tab" href="#prj-mains-tag-3" role="tab" aria-controls="prj-mains-tag-3" aria-selected="false"> Beauty </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-4-tab" data-toggle="tab" href="#prj-mains-tag-4" role="tab" aria-controls="prj-mains-tag-4" aria-selected="false"> Event </a>
                    </li>
                    <li>
                        <a title="" class="" id="prj-mains-tag-5-tab" data-toggle="tab" href="#prj-mains-tag-5" role="tab" aria-controls="prj-mains-tag-5" aria-selected="false"> TVC </a>
                    </li>
                </ul>
                <div class="tab-content content-prj__tag">
                    <div class="tab-pane fade show active" id="prj-mains-tag-1" role="tabpanel" aria-labelledby="prj-mains-tag-1-tab">
                        <div class="row mb-30s">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-1.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-2.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-3.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-4.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-5.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-1.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-2.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-3.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-4.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-5.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-1.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-2.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-3.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-4.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-5.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-1.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-2.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-3.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-4.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-5.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="item-prj__main">
                                    <div class="videos-prj__main">
                                        <img src="theme/assets/images/img-item-prj-5.png" alt="">
                                        <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                            <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="intros-prj__main">
                                        <div class="text-prj__main">
                                            <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                            <p class="fs-13s">TVC</p>
                                        </div>
                                        <div class="btn-prj__item">
                                            <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                            <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-2" role="tabpanel" aria-labelledby="prj-mains-tag-2-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-3" role="tabpanel" aria-labelledby="prj-mains-tag-3-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-4" role="tabpanel" aria-labelledby="prj-mains-tag-4-tab">
                    </div>
                    <div class="tab-pane fade" id="prj-mains-tag-5" role="tabpanel" aria-labelledby="prj-mains-tag-5-tab">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-prj__pages mb-150s">
        <div class="container">
            <h2 class="title-hara titles-center__alls color-blues fs-38s mb-40s">5000+ đối tác đã tin tưởng hợp tác</h2>
        </div>
        <div class="list-partner__prj">
            <div class="container">
                <div class="row gutter-100">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                        <div class="item-patner__main">
                            <img src="theme/assets/images/img-partner-main-1.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <form class="mb-30s">
                <div class="text-top__main titles-center__alls mb-35s">
                    <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                </div>
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>