<?php @include('header.php'); ?>
<main>
    <section class="banner-about__pages mb-130s">
        <div class="container">
            <div class="intros-banner__about">
                <h2 class="title-hara fs-40s mb-10s">Hành trình nâng tầm thương hiệu Việt</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
            </div>
        </div>
        <div class="img-banner__about">
            <img src="theme/assets/images/img-banner-about-pages.png" alt="">
        </div>
    </section>
    <section class="reason-mains reason-about mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-115s">
                <h2 class="title-hara color-blues fs-38s mb-30s">Onemore - Không bao giờ dừng lại </h2>
                <p>Thành lập vào năm 2021 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
            <div class="list-parameter__reason">
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">15000+</h3>
                    <p class="fs-21s title-rb__bold">Khách hàng đã tin tưởng hợp tác</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">10+</h3>
                    <p class="fs-21s title-rb__bold">10 năm kinh nghiệm</p>
                </div>
                <div class="items-parameter__reason">
                    <h3 class="title-hara color-blues fs-78s">20+</h3>
                    <p class="fs-21s title-rb__bold">Nhân sự tài năng</p>
                </div>
            </div>
        </div>
    </section>
    <section class="why-about__page mb-150s">
        <div class="img-why__pages">
            <img src="theme/assets/images/img-why-pages-1.png" alt="">
        </div>
        <div class="content-why__page mb-80s">
            <div class="container">
                <div class="text-top__main titles-center__alls mb-20s">
                    <h2 class="title-hara fs-38s mb-20s">Tại sao chọn chúng tôi? </h2>
                    <p>Tiền thân là một Production House, hơn ai hết, Onemore hiểu rõ tầm quan trọng của media trong việc truyền thông, quảng cáo. Quý khách hàng hoàn toàn yên tâm khi lựa chọn Onemore, chúng tôi tự tin tạo ra những sản phẩm truyền thông được ưa chuộng và đem lại hiệu quả cao cho các chiến dịch marketing với:</p>
                </div>
                <div class="list-why__page mb-40s">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="item-why__page">
                                <img src="theme/assets/images/img-why-page-item-1.png" alt="">
                                <h3 class="title-why__page title-rb__bold mb-20s fs-21s">Tâm huyết cùng trách nhiệm</h3>
                                <p class="title-rb__medium fs-14s">Khát khao cống hiến xây dựng ý tưởng, làm tròn trách nhiệm khi thực thi.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="item-why__page">
                                <img src="theme/assets/images/img-why-page-item-2.png" alt="">
                                <h3 class="title-why__page title-rb__bold mb-20s fs-21s">Sáng tạo để khác biệt</h3>
                                <p class="title-rb__medium fs-14s">Mở ra hướng đi mới cho từng giải pháp marketing để thay đổi thế giới bằng sự khác biệt và sức sáng tạo.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="item-why__page">
                                <img src="theme/assets/images/img-why-page-item-3.png" alt="">
                                <h3 class="title-why__page title-rb__bold mb-20s fs-21s">Sự chuyên nghiệp hàng đầu</h3>
                                <p class="title-rb__medium fs-14s">Sở hữu đội ngũ ekip với gần 10 năm kinh nghiệm, Onemore tự tin là đơn vị truyền thông cung cấp các giải pháp marketing có chất lượng trong top đầu miền Trung.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="item-why__page">
                                <img src="theme/assets/images/img-why-page-item-4.png" alt="">
                                <h3 class="title-why__page title-rb__bold mb-20s fs-21s">Đích đến là sự hiệu quả</h3>
                                <p class="title-rb__medium fs-14s">Onemore luôn nỗ lực mang lại nhiều giá trị vượt trội cho khách hàng, hỗ trợ hoạt động kinh doanh của công ty.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="register-sevice__main">
                    <h3 class="title-hara  fs-22s mb-10s">Đồng hành cùng Onemore và nâng tầm
                        doanh nghiệp của bận ngay từ bây giờ!</h3>
                    <a href="#form-quote-main" title="" class="btn-blue__trans btn-to__form titles-transform__alls">ĐĂNG KÝ DỊCH VỤ</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="list-core__sevice">
                <div class=" items-core__sevice">
                    <div class="content-items__core">
                        <div class="text-items__core">
                            <h3 class="title-hara color-blues fs-38s mb-20s">Triết lý kinh doanh</h3>
                            <p>Không bao giờ dừng lại là tôn chỉ của Onemore Agency. Chúng tôi tin rằng chỉ cần cố gắng thêm một lần nữa, mọi vấn đề dù khó khăn đến đâu đều có thể được giải quyết. Onemore luôn nỗ lực và không ngừng tạo ra những giá trị vượt trội về truyền thông cho doanh nghiệp và cộng đồng.</p>
                        </div>
                        <div class="img-items__core">
                            <span>
                                <img src="theme/assets/images/img-items-core-1.png" alt="">
                            </span>
                        </div>
                    </div>
                </div>
                <div class=" items-core__sevice">
                    <div class="content-items__core">
                        <div class="text-items__core">
                            <h3 class="title-hara color-blues fs-38s mb-20s">Tầm nhìn</h3>
                            <p>Trở thành công ty truyền thông số 1 miền Trung vào năm 2025, hướng đến vị thế hàng đầu Việt Nam và vươn ra thế giới.</p>
                        </div>
                        <div class="img-items__core">
                            <span>
                                <img src="theme/assets/images/img-items-core-1.png" alt="">
                            </span>
                        </div>
                    </div>
                </div>
                <div class=" items-core__sevice">
                    <div class="content-items__core">
                        <div class="text-items__core">
                            <h3 class="title-hara color-blues fs-38s mb-20s">Sứ mệnh</h3>
                            <p>Nâng tầm giá trị doanh nghiệp, đồng hành cùng doanh nghiệp trên con đường phát triển bền vững.</p>
                        </div>
                        <div class="img-items__core">
                            <span>
                                <img src="theme/assets/images/img-items-core-1.png" alt="">
                            </span>
                        </div>
                    </div>
                </div>
                <div class=" items-core__sevice">
                    <div class="content-items__core">
                        <div class="text-items__core">
                            <h3 class="title-hara color-blues fs-38s mb-20s">Giá trị cốt lõi</h3>
                            <div class="tag-prj__alls">
                                <ul class="nav tag-prj__mains mb-70s" id="myTab" role="tablist">
                                    <li>
                                        <a title="" class=" active" id="prj-mains-tag-1-tab" data-toggle="tab" href="#prj-mains-tag-1" role="tab" aria-controls="#prj-mains-tag-1" aria-selected="true"> Biết ơn
                                        </a>
                                    </li>
                                    <li>
                                        <a title="" class="" id="prj-mains-tag-2-tab" data-toggle="tab" href="#prj-mains-tag-2" role="tab" aria-controls="prj-mains-tag-2" aria-selected="false"> Thấu hiểu </a>
                                    </li>
                                    <li>
                                        <a title="" class="" id="prj-mains-tag-3-tab" data-toggle="tab" href="#prj-mains-tag-3" role="tab" aria-controls="prj-mains-tag-3" aria-selected="false"> Tận tâm </a>
                                    </li>
                                    <li>
                                        <a title="" class="" id="prj-mains-tag-4-tab" data-toggle="tab" href="#prj-mains-tag-4" role="tab" aria-controls="prj-mains-tag-4" aria-selected="false"> Tin cậy </a>
                                    </li>
                                    <li>
                                        <a title="" class="" id="prj-mains-tag-5-tab" data-toggle="tab" href="#prj-mains-tag-5" role="tab" aria-controls="prj-mains-tag-5" aria-selected="false"> Đồng hành </a>
                                    </li>
                                </ul>
                                <div class="tab-content content-prj__tag">
                                    <div class="tab-pane fade show active" id="prj-mains-tag-1" role="tabpanel" aria-labelledby="prj-mains-tag-1-tab">
                                        <p>Với Onemore, mỗi cơ hội được hợp tác và làm việc cùng doanh nghiệp là một cơ hội được thể hiện năng lực; được học hỏi; được trải nghiệm và được phát triển.</p>
                                    </div>
                                    <div class="tab-pane fade" id="prj-mains-tag-2" role="tabpanel" aria-labelledby="prj-mains-tag-2-tab">
                                        <p>Với Onemore, mỗi cơ hội được hợp tác và làm việc cùng doanh nghiệp là một cơ hội được thể hiện năng lực; được học hỏi; được trải nghiệm và được phát triển.</p>
                                    </div>
                                    <div class="tab-pane fade" id="prj-mains-tag-3" role="tabpanel" aria-labelledby="prj-mains-tag-3-tab">
                                        <p>Với Onemore, mỗi cơ hội được hợp tác và làm việc cùng doanh nghiệp là một cơ hội được thể hiện năng lực; được học hỏi; được trải nghiệm và được phát triển.</p>
                                    </div>
                                    <div class="tab-pane fade" id="prj-mains-tag-4" role="tabpanel" aria-labelledby="prj-mains-tag-4-tab">
                                        <p>Với Onemore, mỗi cơ hội được hợp tác và làm việc cùng doanh nghiệp là một cơ hội được thể hiện năng lực; được học hỏi; được trải nghiệm và được phát triển.</p>
                                    </div>
                                    <div class="tab-pane fade" id="prj-mains-tag-5" role="tabpanel" aria-labelledby="prj-mains-tag-5-tab">
                                        <p>Với Onemore, mỗi cơ hội được hợp tác và làm việc cùng doanh nghiệp là một cơ hội được thể hiện năng lực; được học hỏi; được trải nghiệm và được phát triển.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="img-items__core">
                            <span>
                                <img src="theme/assets/images/img-items-core-1.png" alt="">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="trademark-about__page mb-90s">
        <div class="container">
            <div class="text-trademark__about">
                <h2 class="title-hara color-blues fs-38s mb-30s">Thương hiệu Onemore</h2>
                <p>Thành lập vào năm 2021 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
        </div>
    </section>
    <section class="color-meaning mb-190s">
        <div class="container">
            <div class="row gutter-40s">
                <div class="col-lg-5">
                    <div class="text-color__meaning mb-50s">
                        <h3 class="title-hara color-blues fs-38s mb-20s">Ý nghĩa logo</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
                    <div class="text-color__meaning">
                        <h3 class="title-hara color-blues fs-38s mb-20s">Màu sắc</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        <ul class="list-color__meaning">
                            <li>354C9B</li>
                            <li>E21E2B</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="img-color__meaning">
                        <img src="theme/assets/images/img-color-meaning.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="passion-about mb-80s">
        <div class="container">
            <div class="text-trademark__about mb-60s">
                <h2 class="title-hara color-blues fs-38s mb-30s">Luôn đam mê và nỗ lực hết mình</h2>
                <p>Thành lập vào năm 2021 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
            <div class="slide-passion__about">
                <div class="sl-passion__about swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="groups-passion__about">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-1.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Tin</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-2.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Huu Truong</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-3.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Ms. Thao Ly</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-4.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Phong Khac</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-5.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Ms. Trang</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta title-rb__bold fs-20s  mb-15s ">Mục tiêu công việc của bạn là gì?</h3>
                                                <div class="target-personnel__about">
                                                    <ul class="list-target__work">
                                                        <li>Công việc lương cao</li>
                                                        <li>Thời gian linh hoạt</li>
                                                        <li>Đồng đội trẻ nhiệt huyết</li>
                                                    </ul>
                                                    <a href="tuyendung.php" class="btn-blue__alls">ỨNG TUYỂN NGAY</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="groups-passion__about">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-1.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Tin</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-2.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Huu Truong</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-3.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Ms. Thao Ly</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-4.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Mr. Phong Khac</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                                <img src="theme/assets/images/img-personnel-abouts-5.png" alt="">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta fs-20s title-rb__bold mb-15s ">Ms. Trang</h3>
                                                <p class="fs-13s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                        <div class="item-personnel__about">
                                            <div class="img-personnel__about">
                                            </div>
                                            <div class="intro-personnel__about">
                                                <h3 class="color-penta title-rb__bold fs-20s  mb-15s ">Mục tiêu công việc của bạn là gì?</h3>
                                                <div class="target-personnel__about">
                                                    <ul class="list-target__work">
                                                        <li>Công việc lương cao</li>
                                                        <li>Thời gian linh hoạt</li>
                                                        <li>Đồng đội trẻ nhiệt huyết</li>
                                                    </ul>
                                                    <a href="tuyendung.php" class="btn-blue__alls">ỨNG TUYỂN NGAY</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="group-btns__showss">
                    <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="slide-media__personnel mb-50s">
        <div class="sl-media__personnel swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-1.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-1.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-2.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-2.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-3.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-4.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-4.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-5.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-5.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-6.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-6.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-1.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-1.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="item-media__personnel">
                        <a href="theme/assets/images/img-media-personnel-2.png" title="" data-fancybox="img-media-personnel">
                            <img src="theme/assets/images/img-media-personnel-2.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>