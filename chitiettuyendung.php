<?php @include('header.php'); ?>
<main>
    <section class="content-recuit__detail mb-30s">
        <div class="container">
            <h2 class="title-hara fs-38s mb-20s color-blues">Account Manager ( Thực Tập Sinh )</h2>
            <ul class="intro-item__recruit mb-40s">
                <li>
                    <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                </li>
                <li>
                    <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                </li>
                <li>
                    <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                </li>
            </ul>
            <div class="text-recuit__detail">
                <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls">
                    MÔ TẢ CÔNG VIỆC ACCOUNT MANAGER
                </h3>
                <ul>
                    <li>Onemore là công ty chuyên sâu về hoạch định chiến lược, thiết kế nhận diện và truyền thông thương hiệu. Chúng tôi cung cấp một giải pháp toàn diện giúp doanh nghiệp nâng cao năng lực cạnh tranh thông qua việc xây dựng thương hiệu mạnh.</li>
                    <li>Onemore là công ty chuyên sâu về hoạch định chiến lược, thiết kế nhận diện và truyền thông thương hiệu. Chúng tôi cung cấp một giải pháp toàn diện giúp doanh nghiệp nâng cao năng lực cạnh tranh thông qua việc xây dựng thương hiệu mạnh.</li>
                    <li>Onemore là công ty chuyên sâu về hoạch định chiến lược, thiết kế nhận diện và truyền thông thương hiệu. Chúng tôi cung cấp một giải pháp toàn diện giúp doanh nghiệp nâng cao năng lực cạnh tranh thông qua việc xây dựng thương hiệu mạnh.</li>
                    <li>Onemore là công ty chuyên sâu về hoạch định chiến lược, thiết kế nhận diện và truyền thông thương hiệu. Chúng tôi cung cấp một giải pháp toàn diện giúp doanh nghiệp nâng cao năng lực cạnh tranh thông qua việc xây dựng thương hiệu mạnh.</li>
                </ul>
                <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls">
                    YÊU CẦU CÔNG VIỆC
                </h3>
                <ul>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                </ul>
                <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls">
                    QUYỀN LỢI ĐƯỢC HƯỞNG
                </h3>
                <ul>
                    <li>Lương thỏa thuận + Thưởng.</li>
                    <li>Chế độ thưởng doanh số rất cao so với thị trường</li>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                    <li>Có kinh nghiệm quảng cáo đối tượng B2B</li>
                </ul>
                <h3 class="title-rb__bold fs-15s mb-20s titles-transform__alls">
                    THÔNG TIN LIÊN HỆ
                </h3>
                <ul>
                    <li>Liên hệ: Mrs. Thảo Ly.</li>
                    <li>Nộp hồ sơ: Ứng viên nộp CV qua mail : <a href="mailto:hronemore@onemore.vn" title="" class="title-rb__bold"> hronemore@onemore.vn </a></li>
                </ul>
            </div>
            <div class="list-leader__hr">
                <div class="item-leader__hr">
                    <img src="theme/assets/images/img-leader-hr-1.png" alt="">
                    <div class="intros-leader__hr">
                        <p class="title-rb__bold mb-5s ">MR. TÍN</p>
                        <p class="color-text__third">Giám đốc vận hành</p>
                        <p><a href="tel:0964747979" title="" class="fs-20s" class="phone-leader__hr">0964 747 979</a></p>
                    </div>
                </div>
                <div class="item-leader__hr">
                    <img src="theme/assets/images/img-leader-hr-2.png" alt="">
                    <div class="intros-leader__hr">
                        <p class="title-rb__bold mb-5s ">MRS. THẢO LY</p>
                        <p class="color-text__third">Giám đốc vận hành</p>
                        <p><a href="tel:0933765456" title="" class="fs-20s" class="phone-leader__hr">0933 765 456</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="quick-application mb-80s">
        <div class="container">
            <h2 class="title-hara color-blues fs-31s mb-30s">Nộp đơn ứng tuyển</h2>
            <form>
                <div class="row gutter-20">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Email*">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Vị trí ứng tuyển*">
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung mô tả"></textarea>
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Tải lên CV của bạn ( tối đa 5MB
                            </p>
                            <img src="theme/assets/images/img-icons-upload.png">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn-blue__alls titles-transform__alls">ỨNG TUYỂN NGAY</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="list-recruit__pages mb-80s">
        <div class="container">
            <h2 class="title-hara color-blues fs-31s mb-30s">Các vị trí đang tuyển dụng</h2>
            <div class="row gutter-20 mb-60s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="items-recruit__pages">
                        <p class="color-blues mb-20s">VỊ TRÍ TUYỂN DỤNG</p>
                        <h3 class="title-rb__bold fs-18s mb-20s">Account Manager ( Thực Tập Sinh )</h3>
                        <ul class="intro-item__recruit mb-35s">
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-1.png" alt=""> <span class="title-rb__bold">Số lượng:</span> <span class="title-rb__bold color-blues">2</span></p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-3.png" alt=""> <span class="title-rb__bold">Mức lương:</span> <span class="color-blues">VND5.000.000 - 15.000.000</span> </p>
                            </li>
                            <li>
                                <p><img src="theme/assets/images/img-icon-recruit-pages-2.png" alt=""> Cập nhật 1 tháng trước</p>
                            </li>
                        </ul>
                        <a href="chitiettuyendung.php" title="" class="btn-red__alls mb-25s">ỨNG TUYỂN NGAY <img src="theme/assets/images/pen-note-icons.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>