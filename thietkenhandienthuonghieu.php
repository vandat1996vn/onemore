<?php @include('header.php'); ?>
<main>
    <section class="banner-sevice__page mb-80s">
        <img src="theme/assets/images/img-sevice-pages.png" alt="">
        <div class="container">
            <div class="intro-banner__sevice">
                <h2 class="title-hara fs-38s mb-20s">Thiết kế nhận diện thương hiệu</h2>
                <p>Dịch vụ tư vấn và cung cấp chiến lược toàn diện, phù hợp với mô hình kinh doanh để kiến tạo nền tảng phát triển vững mạnh cho doanh nghiệp. </p>
                <a href="#form-quote-main" title="" class="btn-to__form btn-blue__alls  btn-to__form titles-transform__alls">NHẬN TƯ VẤN</a>
            </div>
        </div>
    </section>
    <section class="brand-partner__pages mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-3">
                    <h2 class="title-hara color-blues fs-26s">5000+ Đối tác đã tin tưởng hợp tác</h2>
                </div>
                <div class="col-lg-9">
                    <div class="sl-brand__partner swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-1.png">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="items-brand__partner">
                                    <img src="theme/assets/images/brand-partner-2.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="prj-brand__pages mb-80s">
        <div class="container">
            <h2 class="title-hara titles-center__alls color-blues fs-38s mb-20s">Dự án đã thực hiện</h2>
            <div class="row mb-60s">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item-prj__main">
                        <div class="videos-prj__main">
                            <img src="theme/assets/images/prj-brand-pages.png" alt="">
                            <a href="https://www.youtube.com/watch?v=N9wlkhaBhgw" class="content-videos__main" data-fancybox="prj-video-main">
                                <img src="theme/assets/images/img-item-sevice-3.png" alt="">
                            </a>
                        </div>
                        <div class="intros-prj__main">
                            <div class="text-prj__main">
                                <h3><a href="#" title="" class="title-prj__main fs-15s">Cá Lửa Restaurant</a></h3>
                                <p class="fs-13s">TVC</p>
                            </div>
                            <div class="btn-prj__item">
                                <p class="item-btn__prj"><i class="fa fa-heart" aria-hidden="true"></i></p>
                                <p class="item-btn__prj"><i class="fa fa-share-alt" aria-hidden="true"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="fs-18s mb-30s titles-center__alls color-text__third ">Bạn muốn tham khảo thêm lĩnh vực khác?</p>
            <a href="#form-quote-main" title="" class="btn-blue__alls btn-to__form titles-transform__alls">NHẬN TƯ VẤN</a>
        </div>
    </section>
    <section class="design-logo__sevice mb-115s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-40s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> <span class="color-red">I. </span>Thiết kế logo</h2>
                <p>Logo không chỉ là một hình ảnh hay biểu tượng mà còn là một yếu tố có tác động mạnh mẽ đến nhận thức thương hiệu, là tín hiệu nhận diện đầu tiên dẫn đến mọi hành vi tiêu dùng của khách hàng.</p>
            </div>
            <div class="standard-design__logo mb-30s">
                <h2 class="title-hara titles-center__alls color-blues fs-38s mb-30s">Tiêu chuẩn thiết kế logo tại Onemore</h2>
                <div class="row gutter-20 justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-1.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Độc đáo và ấn tượng</h3>
                                <p>Mỗi logo đều là duy nhất, không trùng lặp trên thế giới và truyền tải đầy đủ thông điệp của thương hiệu.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-2.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Đầu tư nghiên cứu</h3>
                                <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="item-standard__all">
                            <div class="img-item__standard">
                                <img src="theme/assets/images/img-item-standard-3.png" alt="">
                            </div>
                            <div class="intros-item__standard">
                                <h3 class="title-rb__bold color-blues fs-21s mb-15s">Kỹ thuật tiên tiến</h3>
                                <p>Sử dụng kỹ thuật tốt nhất để thiết kế logo phù hợp với sản phẩm, dịch vụ và bản sắc của thương hiệu.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="process-design__logo">
            <img src="theme/assets/images/bg-process-design-logo.png" alt="">
            <div class="container">
                <h2 class="title-hara titles-center__alls color-blues fs-38s mb-30s">Quy trình thiết kế logo</h2>
                <div class="list-process__all">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">1</p>
                                    <p class="titles-transform__alls">BƯỚC 1</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">NHẬN YÊU CẦU TỪ KHÁCH HÀNG</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">2</p>
                                    <p class="titles-transform__alls">BƯỚC 2</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">KÝ KẾT HỢP ĐỒNG</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">3</p>
                                    <p class="titles-transform__alls">BƯỚC 3</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">NGHIÊN CỨU THỊ TRƯỜNG, THƯƠNG HIỆU VÀ ĐỐI THỦ</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">4</p>
                                    <p class="titles-transform__alls">BƯỚC 4</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">ONEMORE SÁNG TẠO LOGO</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">5</p>
                                    <p class="titles-transform__alls">BƯỚC 5</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">GỬI FILE LOGO CHO KHÁCH HÀNG</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">6</p>
                                    <p class="titles-transform__alls">BƯỚC 6</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">NHẬN PHẢN HỒI VÀ HIỆU CHỈNH</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">7</p>
                                    <p class="titles-transform__alls">BƯỚC 7</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">BÀN GIAO VÀ NGHIỆM THU DỰ ÁN</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="design-logo__sevice mb-80s">
        <div class="container">
            <div class="text-top__main titles-center__alls mb-30s">
                <h2 class="title-hara color-blues fs-38s mb-10s"> <span class="color-red">II. </span>Thiết kế bộ nhận diện thương hiệu</h2>
                <p>Bộ nhận diện thương hiệu là tập hợp những yếu tố hữu hình của thương hiệu bao gồm tên gọi, logo, màu sắc, yếu tố đồ họa, tài liệu marketing, hồ sơ nhân lực,... Đây được coi là là yếu tố giúp doanh nghiệp trở nên độc đáo, khác biệt với các đối thủ cạnh tranh trên thị trường.</p>
            </div>
            <div class="benefit-design__trademark mb-40s">
                <h2 class="title-hara color-blues titles-center__alls fs-38s mb-60s">Lợi ích khi có bộ nhận diện thương hiệu</h2>
                <div class="row gutter-40">
                    <div class="col-lg-5">
                        <div class="img-design__trademark">
                            <img src="theme/assets/images/benefit-design-trademark.png">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="intros-design__trademark">
                            <div class="row gutter-40">
                                <div class="col-lg-6">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Xây dựng bản sắc thương hiệu</h3>
                                        <p>Bản sắc thương hiệu là yếu tố giúp doanh nghiệp tạo được dấu ấn riêng, giúp để lại ấn tượng sâu sắc trong tâm trí khách hàng.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Gia tăng doanh thu & lợi nhuận</h3>
                                        <p>Bộ nhận diện thương hiệu chính là thước đo để khách hàng có thể uy tín cũng như sự chuyên nghiệp của doanh nghiệp, giúp chạm đến trái tim của khách hàng dễ dàng, từ đó khách hàng sẽ tin tưởng và lựa chọn sản phẩm.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Thuận tiện cho chiến lược marketing</h3>
                                        <p>Bộ nhận diện thương hiệu giúp doanh nghiệp dễ dàng và tiết kiệm thời gian trong việc triển khai chiến dịch marketing do đã xây dựng lòng tin với khách hàng từ trước. </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="items-design__trademark mb-20s">
                                        <h3 class="title-design__trademark title-rb__bold color-blues fs-21s mb-10s">Tạo lợi thế cạnh tranh</h3>
                                        <p>Một bộ nhận diện tốt, dễ nhớ, dễ cảm nhận sẽ tạo ra sức mạnh cho thương hiệu và khi sản phẩm của thương hiệu được tung ra sẽ nhận được sự chào đón nồng nhiệt của khách hàng.</p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-to__form btn-red__alls" href="#form-quote-main">ĐĂNG KÝ NGAY!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="process-design__logo">
            <img src="theme/assets/images/bg-benefit-design-trademark.png" alt="">
            <div class="container">
                <h2 class="title-hara titles-center__alls color-blues fs-38s mb-30s">Quy trình thiết kế logo</h2>
                <div class="list-process__all">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">1</p>
                                    <p class="titles-transform__alls">BƯỚC 1</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">TIẾP NHẬN YÊU CẦU TỪ KHÁCH HÀNG</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">2</p>
                                    <p class="titles-transform__alls">BƯỚC 2</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">KÝ KẾT HỢP ĐỒNG</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">3</p>
                                    <p class="titles-transform__alls">BƯỚC 3</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">NGHIÊN CỨU THỊ TRƯỜNG, THƯƠNG HIỆU VÀ ĐỐI THỦ</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">4</p>
                                    <p class="titles-transform__alls">BƯỚC 4</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">TƯ VẤN XÂY DỰNG BỘ NHẬN DIỆN THƯƠNG HIỆU</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">5</p>
                                    <p class="titles-transform__alls">BƯỚC 5</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">THIẾT KẾ VÀ SÁNG TẠO BỘ NHẬN DIỆN THƯƠNG HIỆU</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">6</p>
                                    <p class="titles-transform__alls">BƯỚC 6</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">TRÌNH BÀY, NHẬN PHẢN HỒI VÀ HIỆU CHỈNH</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">7</p>
                                    <p class="titles-transform__alls">BƯỚC 7</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">ĐÓNG GÓI BỘ NHẬN DIỆN</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                            <div class="items-process__all">
                                <div class="title-items__process">
                                    <p class="number-items__process">8</p>
                                    <p class="titles-transform__alls">BƯỚC 8</p>
                                </div>
                                <div class="intro-items__process">
                                    <h3 class="title-rb__bold fs-18s mb-10s">BÀN GIAO VÀ NGHIỆM THU DỰ ÁN</h3>
                                    <p>Onemore luôn nghiên cứu thị trường và đối thủ để có thể học hỏi và cải tiến từ những sai sót của đối thủ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feekback-main mb-115s">
        <div class="container">
            <div class="row gutter-100">
                <div class="col-lg-4">
                    <div class="text-feedback__main">
                        <h2 class="title-hara fs-38s mb-30s">Khách hàng nói gì về Onemore</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
                    </div>
                    <div class="group-btns__showss">
                        <div class="showss-button-prev"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                        <div class="showss-button-next"><img src="theme/assets/images/arrow-btn-slide.png"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="feedback-slide__main swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-1.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-2.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="item-feedback__main">
                                    <img src="theme/assets/images/quocte-feedback.png" alt="">
                                    <p class="mb-30s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy... </p>
                                    <div class="box-customer__feedback">
                                        <img src="theme/assets/images/img-feedback-main-3.png" alt="">
                                        <div class="info-customer__feedback">
                                            <h3 class="title-rb__bold fs-18s color-penta">Duy Hoang</h3>
                                            <p class="fs-13s">Co Fouder Tre Xanh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container mb-115s">
        <div class="contact-main" id="form-quote-main">
            <div class="text-top__main titles-center__alls mb-35s">
                <h2 class="title-hara color-blues fs-38s mb-10s">Kết nối với chúng tôi</h2>
                <p>Lorem ipsum dolor sit amet, consectetuzer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut</p>
            </div>
            <form class="mb-30s">
                <div class="row gutter-20">
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Họ và tên*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Số điện thoại*">
                    </div>
                    <div class="col-lg-12">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Địa chỉ e-mail*">
                    </div>
                    <div class="col-lg-12">
                        <p class="fs-19s mb-10s color-blues">Lựa chọn dịch vụ</p>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Branding
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Marcom
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 col-12">
                        <div class="check-contact__alls">
                            <input type="radio" class="form-check-input input-checked" value="option1" id="exampleCheck1" name="exampleRadios" checked="">
                            <label class="form-check-label" for="exampleRadios1">
                                Giải pháp marketing tổng thể
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="" class="control-alls input-alls" placeholder="Yêu cầu dịch vụ*">
                    </div>
                    <div class="col-lg-6">
                        <div class="up-file__prd">
                            <input type="file" name="" class="input-files">
                            <p class="btn-Choose__file">
                                Gửi file đính kèm
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea rows="4" class="control-alls" placeholder="Nội dung yêu cầu..."></textarea>
                    </div>
                </div>
                <button class="btn-blue__alls titles-transform__alls ">GỬI YÊU CẦU</button>
            </form>
            <ul class="info-contact__main fs-19s">
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Hotline<a href="tel:0933546453" title="">0933 546 453</a></p>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <p>E-mail: <a href="mailto:hronemore@onemore.com" title="">hronemore@onemore.com</a></p>
                </li>
            </ul>
        </div>
    </section>
</main>
<?php @include('footer.php'); ?>